
import React, { Component } from 'react';
import { View } from 'react-native';
import { init, dispatch } from '@rematch/core';
import { Provider } from 'react-redux';
import createReactNavigationPlugin from '@rematch/react-navigation'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import Routes from './AppRouteConfigs'
import * as models from './src/store/models';
import FlashMessage from "react-native-flash-message";

console.disableYellowBox = true;

// add react navigation with redux
const { Navigator, reactNavigationPlugin } = createReactNavigationPlugin({
  Routes,
  initialScreen: 'Root'
})

let RootStack = createStackNavigator({
  Routes,
  initialScreen: 'Root'
}, {
  defaultNavigationOptions: {
    header: null
  },
}
);

let Navigation = createAppContainer(RootStack);

const store = init({
  models,
  plugins: [reactNavigationPlugin],
})

export default class App extends Component {
  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Provider store={store}>
          <Navigation />
        </Provider>
        <FlashMessage style={{ margin: 16, elevation: 3, borderRadius: 16, borderWidth: 1, borderColor: 'white' }} position="bottom" />
      </View>
    )
  }

}
