import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, AsyncStorage, Dimensions, Image, ScrollView, StatusBar, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import InputWithLabel from '../component/InputWithLabel';
import Header from '../component/Header';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';

const { width } = Dimensions.get('window')

class SurveyCarwash extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.rootState = this.root.state.params;
        this.state = {
            isLoading: false,

        }
    }

    componentDidMount() {
        // AsyncStorage.setItem('SignInIntro', '')

    }
    sendSurvey(){
        null
    }
    render() {
        let deviceId = this.root.state.deviceId
        return (
            <View style={[Styles.container]}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                <Header
                    parent={this}
                    title="Car wash"
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: Color.grayWhite }}
                    keyboardShouldPersistTaps='always'>

                    <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>
                        <Image
                            source={require('../../../img/undraw_in_progress.png')}
                            style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 1.5, height: width / 2, marginVertical: 20 }]}
                        />
                        {this.state.isLoading == true && (
                            <ActivityIndicator />
                        )}
                        {/* <NoData iconName={"emoticon-tongue"}><Text>Pilih Ujian</Text></NoData> */}
                        {this.state.isLoading == false && (
                            <View>
                                <Button
                                    labelButton="Saya menginginkan layanan ini"
                                    onPress={() => this.sendSurvey()}
                                    backgroundColor={Color.primary}
                                // iconLeft={true}
                                // iconLeftName="md-eye"
                                />
                            </View>
                        )}
                    </View>
                </ScrollView>
            </View>
        )
    }
}


function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SurveyCarwash);