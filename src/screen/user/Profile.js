import React, { Component } from 'react';
import { ActivityIndicator, Clipboard, Dimensions, RefreshControl, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import ImageContent from '../component/ImageContent';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import { withNavigation } from 'react-navigation';
import Url from '../utility/Url.js';
import { dispatch as Redux } from '@rematch/core';

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATTITUDE_DELTA = 0.004757
const LONGTITUDE_DELTA = LATTITUDE_DELTA * ASPECT_RATIO

class Profile extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        // this.userData = this.user.user;
        this.state = {
            isLoading: false,
            modalLoading: false,
            latitude: '',
            longitude: '',
            sendingOtp: false,
            // nama_lengkap: this.userData.name != null ? this.userData.name : '',
            // no_telp: this.userData.phone != null ? this.userData.phone : null,
            // no_telp_baru: this.userData.phone != null ? this.userData.phone : null,
            // username: this.userData.email != null ? this.userData.email : '',
            // rating: this.userData.rating != null ? this.userData.rating : '',
            lokasi: '',

            imageIsLoading: false,
            downloadUrl: '',
            // image: this.userData.photo_url ? this.userData.photo_url : '',
            imageName: '',
            defaultImage: require('../../../img/default_empty.png'),
            defaultCover: require('../../../img/default_cover.jpg'),

            // imageCover: this.userData.photo_cover ? this.userData.photo_cover : '',
            // gender: this.userData.gender ? this.userData.gender : 'L'

        }
        Redux.component.setProfile(this);

    }
    _refresh() {
        this.componentDidMount();
        // alert("A")
        // this.setState({ isLoading: true });
    }
    writeToClipboard = async () => {
        await Clipboard.setString(this.state.kode_refferalku);
        this.root.message('Copied', 'success');
    };
    componentDidMount() {
        //console.log('data user', this.userData)
        // this.getProfile(this.userData.token)
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            // this.getProfile(this.userData.token)

        });
    }
    componentWillUnmount() {
        this.focusListener.remove();
    }
    getProfile(token) {
        Url.getProfile(token).then((response) => {
            let data = response.data.data;
            data.token = token
            console.log('data get profile:', data);
            let dataUser = data.user
            console.log('data.user:', dataUser);
            Redux.component.setUser(data);
            if (dataUser != null) {
                this.setState({
                    nama_lengkap: dataUser.name != null ? dataUser.name : '',
                    no_telp: dataUser.phone != null ? dataUser.phone : '',
                    no_telp_baru: dataUser.phone ? dataUser.phone : '',
                    username: dataUser.email != null ? dataUser.email : '',
                    rating: dataUser.rating != null ? dataUser.rating : '',
                    image: dataUser.photo_url ? dataUser.photo_url : '',
                })
            }
            this.setState({ isLoading: false, });
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.setState({ isLoading: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    help() {
        this.root.navigate("WebViewPage", { title: "Help", name: 'help' })
    }
    about() {
        this.root.navigate("WebViewPage", { title: "About", name: 'about' })
    }
    termsOfService() {
        this.root.navigate("WebViewPage", { title: "Terms of service", name: 'service' })
    }
    termsAndConditions() {
        this.root.navigate("WebViewPage", { title: "Terms & Conditions", name: 'terms' })
    }
    render() {
        let { image, nama_lengkap, rating, username, no_telp, defaultImage } = this.state
        return (
            <View style={Styles.container}>
                {/* <Header parent={this}
                    title={"My Profile"}
                /> */}
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.state.isLoading == false && (
                    <ScrollView
                        style={{ flex: 1, paddingBottom: 70 }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }>
                        <View style={{ flex: 1 }}>
                            {this.state.buttonLoading == true && (
                                <ActivityIndicator size={"large"} />
                            )}
                            <ImageContent
                                onPress={() => this.root.navigate("EditProfile")}
                                backgroundColor={Color.grayWhite}
                                nameTextColor={Color.primaryFont}
                                sourceImage={image != null && image.search("://") >= 0 ? { uri: image } : defaultImage}
                                imageSize={64}
                                radiusImage={10}
                                name={nama_lengkap}
                                childText={username}
                                otherText={no_telp != null ? no_telp : null}
                                childTextColor={Color.primaryFont}
                                rateTextBackColor={Color.primary}
                                rateText={rating ? rating : '5.0'}
                                iconRight={true}
                                iconColor={Color.primaryFont}
                                materialIconRightName="settings"
                            />
                            <View style={Styles.content}>
                                <TouchableOpacity onPress={() => this.help()} style={{ flex: 1, paddingVertical: 8, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                    <TextButton
                                        onPress={() => this.help()}
                                        labelButton={"Help"}
                                        color={Color.black}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.termsOfService()} style={{ flex: 1, paddingVertical: 8, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                    <TextButton
                                        onPress={() => this.termsOfService()}
                                        labelButton={"Terms of Service"}
                                        color={Color.black}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.termsAndConditions()} style={{ flex: 1, paddingVertical: 8, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                    <TextButton
                                        onPress={() => this.termsAndConditions()}
                                        labelButton={"Terms & Conditions"}
                                        color={Color.black}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.about()} style={{ flex: 1, paddingVertical: 8, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                    <TextButton
                                        onPress={() => this.about()}
                                        labelButton={"About"}
                                        color={Color.black}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>
                        {/*<View style={{ height: 60 }} /> */}
                    </ScrollView>
                )}
                <Button
                    containerStyle={{ borderRadius: 0, position: 'absolute', bottom: 0, right: 0, left: 0 }}
                    labelButton="Keluar"
                    backgroundColor={Color.redFill}
                    onPress={() => this.root.logOut(this.root.state.deviceId)}
                // iconLeft={true}
                // iconLeftName="md-eye"
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        flex: 1, height: 50, fontSize: 13, borderWidth: 1, borderColor: Color.blackOpacity,
        backgroundColor: Color.white, paddingHorizontal: 10,
        marginBottom: 5, borderRadius: 30
        // elevation: 3
    },
    picker: {
        width: '100%', height: 50,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFontFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
const ThisScreen = connect(
    mapStateToProps
)(Profile);
export default withNavigation(ThisScreen);