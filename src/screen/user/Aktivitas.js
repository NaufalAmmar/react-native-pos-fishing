import { dispatch as Redux } from '@rematch/core';
import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Linking, Modal, Image, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import StarRating from 'react-native-star-rating';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import Button from '../component/Button';
import ImageContent from '../component/ImageContent';
import InputWithLabel from '../component/InputWithLabel';
import LoadingImage from '../component/LoadingImage';
import NoData from '../component/NoData';
import TextButton from '../component/TextButton';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATTITUDE_DELTA = 0.004757
const LONGTITUDE_DELTA = LATTITUDE_DELTA * ASPECT_RATIO

class Aktivitas extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        this.userData = this.user.user;
        this.state = {
            isLoading: false,
            modalLoading: false,
            aktivitas: [],
            startCamera: false,
            QR_Code: '',
            dataSelected: null,
            defaultImage: require('../../../img/default_empty.png'),
            rating: 0,
            comment: '',
        }
        Redux.component.setAktivitas(this);
    }
    _refresh() {
        this.setState({ aktivitas: [] })
        this.getOrder()
    }
    onQR_Code_Scan_Done = (QR_Code) => {

        this.setState({ QR_Code_Value: QR_Code });
        console.log(QR_Code)
        this.setState({ startCamera: false });
        this.checkIn(QR_Code)
    }
    checkIn(QR_Code) {
        let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')

        let { dataSelected } = this.state
        this.setState({ isLoading: true })
        Url.scanCheckIn(this.userData.token, dataSelected.id, QR_Code, now).then((response) => {
            let data = response.data.data;
            if (data.layanan != null) {
                this.setState({ layanan: data.layanan });
                this.root.message(data.message, 'success');
            }
            this._refresh()
            this.setState({ isLoading: false, startCamera: false });
            // console.log('data get layanan:', data);
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false, startCamera: false });
            }
            this.setState({ isLoading: false, startCamera: false });
            this.root.message(error.response.data.message, 'warning');
        });
    }
    scanBarcode() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.startCamera}
                onRequestClose={() => this.setState({ startCamera: false })}
                // onRequestClose={() => this.setState({ modalLengkapi: false })}
                style={{ flex: 1 }}
            >
                <View style={{ flex: 1, backgroundColor: Color.primary }}>
                    <CameraKitCameraScreen
                        showFrame={false}
                        scanBarcode={true}
                        laserColor={'#FF3D00'}
                        frameColor={Color.primary}
                        colorForScannerFrame={'black'}
                        cameraOptions={{
                            // flashMode: 'auto',             // on/off/auto(default)
                            focusMode: 'on',               // off/on(default)
                            zoomMode: 'on',                // off/on(default)
                            ratioOverlay: '1:1',            // optional, ratio overlay on the camera and crop the image seamlessly
                            ratioOverlayColor: '#00000077' // optional
                        }}

                        onReadCode={event =>
                            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
                        }
                    />
                </View>
                <View style={[styles.overlay, styles.topOverlay]}>
                    <Text style={styles.scanScreenMessage}>Please scan the QR code for Check In.</Text>
                    {/* {this.state.data_qrcode != '' && (
                        <Text style={styles.textQrCode}>Code : {this.state.data_qrcode}</Text>
                    )} */}
                </View>
                <View style={[styles.overlayBox, styles.box]}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <Image
                            source={require('../../../img/frame.png')}
                            style={styles.boxImage}
                        />
                    </View>
                </View>

                <View style={[styles.overlay, styles.bottomOverlay]}>
                    <Button
                        onPress={() => { this.setState({ startCamera: false }) }}
                        containerStyle={styles.enterBarcodeManualButton}
                        labelButton="Tutup"
                    />
                </View>

            </Modal >
        )
    }
    componentDidMount() {
        if (this.userData.token == null) {
            this.root.forceOut()
        } else {
            const { navigation } = this.props;
            if (this.state.isLoading == false && this.props.isComponent != true) {
                this.focusListener = navigation.addListener('didFocus', () => {
                    this.getOrder()
                });
            } else {
                this.getOrder()
            }
        }
    }
    componentWillUnmount() {
        if (this.props.isComponent != true) {
            this.focusListener.remove();
        }
    }
    call(no_telp) {
        if (no_telp != null && no_telp.length > 1) {
            const url = `tel:${no_telp}`;
            Linking.canOpenURL(url).then(supported => {
                if (supported) {
                    Linking.openURL(url);
                } else {
                    Alert.alert(
                        'Alert',
                        'Caller is not installed',
                    )
                }
            });
        } else {
            null
        }
    }
    getOrder() {
        let { token } = this.userData
        this.setState({ isLoading: true, aktivitas: [] });
        let dataComponent = []
        Url.getOrder(token).then((response) => {
            let data = response.data.data
            // console.log('data get order:', data);

            let dataOrder = data.order

            if (dataOrder != null) {

                if (this.props.isComponent == true) {

                    if (dataOrder[0] != null) {
                        // console.log('INI', dataOrder[0])
                        if (dataOrder[0].status.id != 2 && dataOrder[0].status.id != 9) {
                            this.setState({ aktivitas: [dataOrder[0]] });
                        }
                    }
                } else {
                    this.setState({ aktivitas: dataOrder });
                }
            }
            this.setState({ isLoading: false });
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.root.message(error.response.data.message, 'warning');
            this.setState({ isLoading: false });
        });
    }
    BeriRating(item) {
        let { customer, status } = item
        let { rating, comment } = this.state
        Alert.alert(
            'Konfirmasi',
            'Kirim kesan kepada capster ini ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.setRatingCustomer(item.id, this.userData.token, customer.id, rating, '', comment).then((response) => {
                            let data = response.data.data;
                            if (data != null) {
                                this.root.message(data.message, 'info');
                            }
                            this.setState({ isLoading: false, rateCustomer: true });
                            this.props.component.riwayat._refresh()
                            // console.log('data rating:', data);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    berangkat(item) {
        let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        Alert.alert(
            'Konfirmasi',
            'Apakah benar anda sedang dalam perjalanan ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.berangkat(this.userData.token, item.id, now).then((response) => {
                            let data = response.data.data;
                            if (data.message != null) {
                                this.root.message(data.message, 'info');
                            }
                            this.setState({ isLoading: false });
                            this._refresh()
                            // console.log('data berangkat:', data);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    mulaiPengerjaan(item) {
        let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        Alert.alert(
            'Konfirmasi',
            'Mulai Pengerjaan ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.mulaiPengerjaan(this.userData.token, item.id, now).then((response) => {
                            let data = response.data.data;
                            if (data != null) {
                                this.root.message(data.message, 'info');
                            }
                            this.setState({ isLoading: false });
                            this._refresh()
                            // console.log('data Mulai:', data);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    selesai(item) {
        let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        Alert.alert(
            'Konfirmasi',
            'Apakah sudah selesai ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.selesaiCukur(this.userData.token, item.id, now).then((response) => {
                            let data = response.data.data;
                            if (data.message != null) {
                                this.root.message(data.message, 'info');
                            }
                            // console.log('data selesai:', data);
                            this.setState({ isLoading: false });
                            this._refresh()
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    placeholder() {
        let content = [{}, {}, {}, {}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    modalLoading() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalLoading}
                onRequestClose={() => null}>
                <TouchableOpacity style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }, Styles.center]}>
                    <Text style={Styles.whiteTextBold}>Sedang mengambil data..</Text>
                    <ActivityIndicator color={Color.white} size={"large"} />
                </TouchableOpacity>
            </Modal>
        )
    }
    lihatSemua() {
        this.root.navigate("AgendaSaya")
    }
    detail(i) {
        this.setState({ dataSelected: i })
        this.root.navigate('CheckIn', i)
    }

    render() {
        let { aktivitas, defaultImage } = this.state
        return (
            <View style={Styles.container}>
                {this.state.isLoading == true && this.props.isComponent != true && (
                    this.placeholder()
                )}
                {this.state.isLoading == false && (
                    <ScrollView
                        style={{ flex: 1, marginVertical: this.props.isComponent != true ? 8 : 0 }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }>
                        {this.scanBarcode()}

                        <View style={Styles.content}>
                            {this.props.isComponent != true && (
                                <View style={Styles.rowBetween}>
                                    <Text style={Styles.blackTextBold}>Aktivitas Saya</Text>
                                    <TextButton
                                        labelButton="Lihat Semua"
                                        onPress={() => this.lihatSemua()}
                                        color={Color.primary}
                                    />
                                </View>
                            )}
                            {this.props.isComponent == true && aktivitas.length != 0 && (
                                <Text style={Styles.blackTextBold}>Sedang Berjalan</Text>
                            )}
                            {Array.isArray(aktivitas) == false || aktivitas.length == 0 && this.props.isComponent != true && (
                                <NoData><Text>Aktivitas Hari ini kosong</Text></NoData>
                            )}
                            <View style={{ width, height: 1, backgroundColor: Color.gray }} />
                            {Array.isArray(aktivitas) && aktivitas.length > 0 &&
                                aktivitas.map((item, index) => {
                                    return (
                                        <View key={index} style={{ marginVertical: 3, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                            <ImageContent
                                                onPress={() => this.detail(item)}
                                                backgroundColor={Color.white}
                                                nameTextColor={Color.primaryFont}
                                                sourceImage={item.customer.photo_url != null && item.customer.photo_url.search("://") >= 0 ? { uri: item.customer.photo_url } : defaultImage}
                                                imageSize={50}
                                                radiusImage={8}
                                                name={item.customer.name}
                                                otherText={moment(item.tanggal_kunjungan).format("dddd, DD MM YYYY - HH:mm")}
                                                rateTextBackColor={Color.primary}
                                                rateText={item.customer.rating ? item.customer.rating : '5.0'}
                                                textRight={true}
                                                circleRight={true}
                                                textRightValue={item.status.id == 1 ? item.status.nama : item.status.id == 7 ? item.status.nama : item.status.id == 6 ? item.status.nama : item.status.nama}
                                                textRightColor={item.status.id == 1 || item.status.id == 9 ? Color.greenFill : Color.blueFill}
                                                detailArray={item.layanan}
                                                priceLayanan={Rupiah.format(item.grand_total)}
                                            />
                                            {/* <Button
                                                labelButton="Detail Order"
                                                onPress={() => this.checkIn(item)}
                                                backgroundColor={Color.secondaryButton}
                                            // iconLeft={true}
                                            // iconLeftName="md-eye"
                                            /> */}
                                            {item.status.id == 15 && (
                                                <Button
                                                    labelButton="Pengerjaan Dimulai"
                                                    onPress={() => this.mulaiPengerjaan(item)}
                                                    backgroundColor={Color.blueFill}
                                                // iconLeft={true}
                                                // iconLeftName="md-eye"
                                                />
                                            )}

                                            {item.status.id == 3 && (
                                                <Button
                                                    labelButton="Pengerjaan Selesai"
                                                    onPress={() => this.selesai(item)}
                                                    backgroundColor={Color.greenFill}
                                                // iconLeft={true}
                                                // iconLeftName="md-eye"
                                                />
                                            )}
                                            {item.status.id == 6 && (
                                                <View style={Styles.rowBetween}>
                                                    <Button
                                                        labelButton={"Check In"}
                                                        containerStyle={{ width: '80%' }}
                                                        onPress={() => this.setState({ dataSelected: item, startCamera: true })}
                                                        backgroundColor={Color.secondaryButton}
                                                    />
                                                    <Button
                                                        // labelButton="Check In"
                                                        containerStyle={{ width: '15%' }}
                                                        onPress={() => this.call(item.customer.phone)}
                                                        backgroundColor={Color.secondaryButton}
                                                        iconLeft={true}
                                                        iconLeftName="md-call"
                                                    />
                                                </View>
                                            )}
                                            {item.status.id == 7 && (
                                                <Button
                                                    labelButton="Saya Berangkat"
                                                    onPress={() => this.berangkat(item)}
                                                    backgroundColor={Color.greenFill}
                                                // iconLeft={true}
                                                // iconLeftName="md-eye"
                                                />
                                            )}
                                        </View>
                                    )
                                })}

                        </View>
                        {/*<View style={{ height: 60 }} /> */}
                    </ScrollView>
                )}
                {this.modalLoading()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        flex: 1, height: 50, fontSize: 13, borderWidth: 1, borderColor: Color.blackOpacity,
        backgroundColor: Color.white, paddingHorizontal: 10,
        marginBottom: 5, borderRadius: 30
        // elevation: 3
    },
    picker: {
        width: '100%', height: 50,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFontFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    container: {
        flex: 1
    },
    box: {
        width: '70%', height: '70%',
        // borderWidth: 2, borderColor: Color.white,
        // backgroundColor: Color.white,
        alignSelf: 'center', marginHorizontal: 20, marginVertical: 0
    },
    boxImage: {
        alignSelf: 'center',
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'contain',
    },
    overlayBox: {
        position: 'absolute',
        // padding: 16,
        // right: '25%',
        // left: '25%',
        top: 20,
        alignSelf: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textQrCode: {
        color: Color.white,
        fontWeight: 'bold',
        fontSize: 20
    },
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center'
    },
    topOverlay: {
        top: 0,
        // flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    bottomOverlay: {
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    enterBarcodeManualButton: {
        padding: 15,
        // backgroundColor: 'white',
        borderRadius: 40
    },
    scanScreenMessage: {
        fontSize: 14,
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }

})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
const ThisScreen = connect(
    mapStateToProps
)(Aktivitas);
export default withNavigation(ThisScreen);
