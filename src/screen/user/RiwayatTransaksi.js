import { dispatch as Redux } from '@rematch/core';
import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Modal, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import StarRating from 'react-native-star-rating';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import Button from '../component/Button';
import ImageContent from '../component/ImageContent';
import InputWithLabel from '../component/InputWithLabel';
import LoadingImage from '../component/LoadingImage';
import NoData from '../component/NoData';
import Color from '../utility/Color.js';
import Rupiah from '../utility/Rupiah.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import Header from '../component/Header';

const { width, height } = Dimensions.get('window')

class RiwayatTransaksi extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        // this.userData = this.user.user
        this.state = {
            isLoading: false,
            modalLoading: false,
            riwayatTransaksi: [],
            dataSelected: null,
            rating: 0,
            comment: '',
            defaultImage: require('../../../img/default_empty.png')
        }

        Redux.component.setRiwayat(this);
    }
    _refresh() {
        this.setState({ isLoading: true })
        this.getRiwayat()
        // this.setState({ isLoading: true });
    }
    componentDidMount() {

        const { navigation } = this.props;
        if (this.state.isLoading == false && this.props.isComponent != true) {
            this.focusListener = navigation.addListener('didFocus', () => {
                // this.getRiwayat()
            });
        } else {
            this.getRiwayat()
        }
    }
    componentWillUnmount() {
        if (this.props.isComponent != true) {
            this.focusListener.remove();
        }
    }

    getRiwayat() {
        let { token } = this.userData
        this.setState({ isLoading: true, riwayatTransaksi: [] });
        let dataComponent = []
        Url.getRiwayat(token).then((response) => {
            let data = response.data.data
            // console.log('data get riwayat:', data);
            let dataOrder = data.order

            if (dataOrder != null) {

                if (this.props.isComponent == true) {
                    dataOrder.map((item) => {
                        if (item.status.id == 2 && item.rating_customer == 0) {
                            dataComponent.push(item)
                        }
                    })
                    if (dataComponent[0] != null) {
                        // console.log("IKI", dataComponent[0])
                        this.setState({ riwayatTransaksi: [dataComponent[0]] });
                    }
                } else {
                    this.setState({ riwayatTransaksi: dataOrder });
                }
            }
            this.setState({ isLoading: false });
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.root.message(error.response.data.message, 'warning');
            this.setState({ isLoading: false });
        });
    }
    BeriRating(item) {
        let { capster, customer, status } = item
        let { rating, comment } = this.state
        Alert.alert(
            'Konfirmasi',
            'Kirim rating kepada customer ini ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.setRatingCustomer(item.id, this.userData.token, capster.id, rating, '', comment).then((response) => {
                            // console.log('data RES:', response);
                            let data = response.data.data;
                            if (data != null) {
                                this.root.message(data.message, 'info');
                            }
                            this._refresh();
                            this.setState({ isLoading: false, rateCustomer: true, rating: 0 });
                            // console.log('data rating:', data);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    sudahDibayar(item) {
        let now = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
        Alert.alert(
            'Konfirmasi',
            'Benar sudah lunas dibayar tunai ?',
            [
                { text: 'Tidak', onPress: () => { this.setState({ isLoading: false }); }, style: 'cancel' },
                {
                    text: 'Ya', onPress: () => {

                        this.setState({ isLoading: true })
                        Url.bayarViaCash(this.userData.token, item.id).then((response) => {
                            let data = response.data.data;
                            if (data != null) {
                                this.root.message(data.message, 'info');
                            }
                            this.setState({ isLoading: false });
                            this._refresh()
                            // console.log('data Mulai:', data);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                this.root.message("Network Error.", 'warning')
                                this.setState({ isLoading: false });
                            }
                            this.setState({ isLoading: false });
                            this.root.message(error.response.data.message, 'warning');
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }
    placeholder() {
        let content = [{}, {}, {}, {}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    modalLoading() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalLoading}
                onRequestClose={() => null}>
                <TouchableOpacity style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }, Styles.center]}>
                    <Text style={Styles.whiteTextBold}>Sedang mengambil data..</Text>
                    <ActivityIndicator color={Color.white} size={"large"} />
                </TouchableOpacity>
            </Modal>
        )
    }
    lihatSemua() {
        this.root.navigate("RiwayatSaya")
    }
    detail(i) {
        this.setState({ dataSelected: i })
        this.root.navigate('CheckIn', i)
    }
    render() {
        let { defaultImage } = this.state
        return (
            <View style={Styles.container}>
                <Header
                    parent={this}
                // title={"Feedback"}
                />
                {this.state.isLoading == true && this.props.isComponent != true && (
                    this.placeholder()
                )}
                {this.state.isLoading == false && (
                    <ScrollView
                        style={{ flex: 1, marginVertical: this.props.isComponent != true ? 8 : 0 }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }>
                        <View style={Styles.content}>
                            {this.props.isComponent != true && (
                                <View style={Styles.rowBetween}>
                                    <Text style={Styles.blackTextBold}>Riwayat Transaksi</Text>
                                    {/* <TextButton
                                        labelButton="Lihat Semua"
                                        onPress={() => this.lihatSemua()}
                                        color={Color.primary}
                                    /> */}
                                </View>
                            )}
                            {this.props.isComponent == true && this.state.riwayatTransaksi.length != 0 && (
                                <Text style={Styles.blackTextBold}>Beri Rating</Text>
                            )}
                            {Array.isArray(this.state.riwayatTransaksi) == false || this.state.riwayatTransaksi.length == 0 && this.props.isComponent != true && (
                                <NoData><Text>Riwayat Hari ini kosong</Text></NoData>
                            )}
                            <View style={{ width, height: 1, backgroundColor: Color.gray }} />
                            {Array.isArray(this.state.riwayatTransaksi) && this.state.riwayatTransaksi.length > 0 &&
                                this.state.riwayatTransaksi.map((item, index) => {
                                    return (
                                        <View key={index} style={{ marginVertical: 3, borderBottomColor: Color.gray, borderBottomWidth: 1 }}>
                                            <ImageContent
                                                onPress={() => this.detail(item)}
                                                backgroundColor={Color.white}
                                                nameTextColor={Color.primaryFont}
                                                sourceImage={item.customer.photo_url != null && item.customer.photo_url.search("://") >= 0 ? { uri: item.customer.photo_url } : defaultImage}
                                                imageSize={50}
                                                radiusImage={8}
                                                name={item.customer.name}
                                                otherText={moment(item.created_at).format("dddd, DD MM YYYY - HH:mm")}
                                                rateTextBackColor={Color.primary}
                                                rateText={item.customer.rating ? item.customer.rating : '5.0'}
                                                textRight={true}
                                                circleRight={true}
                                                textRightValue={item.rating_customer == 0 ? item.status.nama : item.status.id == 7 ? item.status.nama : item.status.id == 6 ? item.status.nama : item.status.nama}
                                                textRightColor={item.status.id == 1 || item.status.id == 9 ? Color.greenFill : Color.blueFill}
                                                detailArray={item.layanan}
                                                priceLayanan={Rupiah.format(item.grand_total)}
                                            />

                                            {item.status.id == 2 && item.rating_customer == 0 && (
                                                <View style={[Styles.center, { flex: 1, paddingVertical: 4, marginVertical: 8, }]}>
                                                    <Text style={{ fontWeight: 'bold', color: Color.black, fontSize: 13, paddingVertical: 8 }}>Bagaimana kesan mu?</Text>
                                                    <StarRating
                                                        // disabled={this.props.disabled}
                                                        emptyStar={'ios-star-outline'}
                                                        fullStar={'ios-star'}
                                                        halfStar={'ios-star-half'}
                                                        iconSet={'Ionicons'}
                                                        maxStars={5}
                                                        rating={this.state.rating}
                                                        selectedStar={(rating) => this.setState({ rating })}
                                                        fullStarColor={Color.yellowFill}
                                                        containerStyle={{ width: 300 }}
                                                    />
                                                    <View style={[Styles.rowBetween, { alignItems: 'center', paddingTop: 8 }]}>
                                                        <InputWithLabel
                                                            style={{ marginRight: 8 }}
                                                            label={"Comment"}
                                                            labelAlign={"center"}
                                                            labelColor={Color.black}
                                                            placeholder="Comment"
                                                            returnKeyType={"next"}
                                                            defaultValue={this.state.comment}
                                                            onChangeText={comment => { this.setState({ comment }) }}
                                                            clearText={() => { this.setState({ comment: '' }) }}
                                                        />
                                                        <Button
                                                            // labelButton="Check In"
                                                            containerStyle={{ width: '15%' }}
                                                            onPress={() => this.BeriRating(item)}
                                                            backgroundColor={Color.secondaryButton}
                                                            iconLeft={true}
                                                            iconLeftName="md-send"
                                                        />
                                                    </View>
                                                </View>
                                            )}
                                            {/* {item.status.id == 9 || item.status.id == 2 && (
                                                <Button
                                                    containerStyle={{ elevation: 0, borderWidth: 1 }}
                                                    labelColor={Color.greenFill}
                                                    borderColor={Color.greenFill}
                                                    iconLeft={true}
                                                    materialCommunityIconLeftName={"coin"}
                                                    labelButton="Sudah Dibayar"
                                                    onPress={() => this.sudahDibayar(item)}
                                                    backgroundColor={'transparent'}
                                                // iconLeft={true}
                                                // iconLeftName="md-eye"
                                                />
                                            )} */}
                                        </View>
                                    )
                                })}

                        </View>
                        {/*<View style={{ height: 60 }} /> */}
                    </ScrollView>
                )}
                {this.modalLoading()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        flex: 1, height: 50, fontSize: 13, borderWidth: 1, borderColor: Color.blackOpacity,
        backgroundColor: Color.white, paddingHorizontal: 10,
        marginBottom: 5, borderRadius: 30
        // elevation: 3
    },
    picker: {
        width: '100%', height: 50,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFontFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
const ThisScreen = connect(
    mapStateToProps
)(RiwayatTransaksi);
export default withNavigation(ThisScreen);
