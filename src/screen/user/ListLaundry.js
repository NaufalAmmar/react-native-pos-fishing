import { dispatch as Redux } from '@rematch/core';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, AsyncStorage, Dimensions, Image, ScrollView, StatusBar, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import InputWithLabel from '../component/InputWithLabel';
import Header from '../component/Header';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';
import { TouchableOpacity } from 'react-native-gesture-handler';

const { width } = Dimensions.get('window')

class ListLaundry extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.rootState = this.root.state.params;
        this.state = {
            isLoading: false,

        }
    }

    componentDidMount() {
        // AsyncStorage.setItem('SignInIntro', '')

    }

    render() {
        let deviceId = this.root.state.deviceId
        return (
            <View style={[Styles.container]}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                <Header
                    parent={this}
                    title="Laundry"
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: Color.grayWhite }}
                    keyboardShouldPersistTaps='always'>

                    <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>

                        {this.state.isLoading == true && (
                            <ActivityIndicator />
                        )}
                        {/* <NoData iconName={"emoticon-tongue"}><Text>Pilih Ujian</Text></NoData> */}
                        {this.state.isLoading == false && (
                            <View style={Styles.column}>
                                <View style={{ flex: 1, backgroundColor: Color.primary, paddingHorizontal: 8, paddingVertical: 8, borderRadius: 4 }}>
                                    <Text style={Styles.whiteText}>Laundry Name</Text>
                                </View>
                                <TouchableOpacity onPress={() => null} style={{ flex: 1, backgroundColor: Color.white, elevation: 2, marginHorizontal: 8, paddingHorizontal: 8, paddingVertical: 8, borderRadius: 2 }}>
                                    <Text style={Styles.primaryFontBold}>Laundry Name</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </ScrollView>
            </View>
        )
    }
}


function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ListLaundry);