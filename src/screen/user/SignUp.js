import RNDateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, AsyncStorage, Dimensions, Image, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Button from '../component/Button';
import Header from '../component/Header';
import InputWithLabel from '../component/InputWithLabel';
import Color from '../utility/Color.js';
import PhotoUploader from '../utility/PhotoUploader';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';

const { width } = Dimensions.get('window')

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.rootState = this.root.state.params;
        this.state = {
            isLoading: false,
            modalIntro: false,
            username: 'jokowi',
            password: '12345678',
            ulangi_password: '12345678',
            showUlangi: true,
            fullname: 'jokowi arifianto',
            ambilFoto: false,
            image: '',
            imageName: '',
            defaultImage: require('../../../img/default_empty.png'),
            alamat_domisili: 'kremil',
            ambilFotoKTP: false,
            imageKTP: '',
            imageNameKTP: '',
            ambilFotoKTPFace: false,
            imageKTPFace: '',
            imageNameKTPFace: '',
            imei: null,
            show: true,
            fontPassword: 14,
            fontPassword2: 35,
            // console: null,
            currentUser: null,
            token: null,
            dataIntro: [
                {
                    step: 1,
                    height: 200,
                    topPosition: 200,
                    description: "Jika kamu sudah punya akun, silahkan masuk disini. "
                },
                {
                    step: 2,
                    height: 50,
                    topPosition: 440,
                    description: "Jika kamu belum punya akun, silahkan daftar disini. "
                },
                {
                    step: 3,
                    height: 50,
                    topPosition: 510,
                    description: "atau kamu juga bisa menggunakan Facebook dan Google untuk melanjutkan. "
                },
            ],
            dataGender: [
                { kode: "L", name: "Laki-laki" },
                { kode: "P", name: "Perempuan" },
            ],
            gender: 'L',
            page: 0,
            calendar: false,
            no_telp: '0897391818',
            tempat_lahir: 'Soerabaja',
            tanggal_lahir: new Date(1990, 0, 1)
        }
    }

    componentDidMount() {
        // AsyncStorage.setItem('SignUpIntro', '')
        this.cekStorageIntro();

    }
    ambilFoto() {
        this.setState({ ambilFoto: true });
        PhotoUploader.upload('foto_profil', this.state.username).then((result) => {
            // console.log('hasil ambil foto', result);
            this.setState({ ambilFoto: false, image: result.data, imageName: result.imageName });
        }).catch((error) => {
            this.setState({ ambilFoto: false });

        });
    }
    ambilFotoKTP() {
        this.setState({ ambilFotoKTP: true });
        PhotoUploader.upload('foto_ktp', this.state.username).then((result) => {
            // console.log('hasil ambil foto ktp', result);
            this.setState({ ambilFotoKTP: false, imageKTP: result.data, imageNameKTP: result.imageName });
        }).catch((error) => {
            this.setState({ ambilFotoKTP: false });

        });
    }
    ambilFotoKTPFace() {
        this.setState({ ambilFotoKTPFace: true });
        PhotoUploader.upload('foto_ktp_face', this.state.username).then((result) => {
            // console.log('hasil ambil foto ktp', result);
            this.setState({ ambilFotoKTPFace: false, imageKTPFace: result.data, imageNameKTPFace: result.imageName });
        }).catch((error) => {
            this.setState({ ambilFotoKTPFace: false });

        });
    }
    async cekStorageIntro() {
        try {
            let isIntro = await AsyncStorage.getItem('SignUpIntro');
            if (isIntro == null) {
                this.setState({ modalIntro: true })
            } else {
                this.setState({ modalIntro: false })
            }
        } catch (error) {
            // Error retrieving data
            // console.log('error:', error);
        }
    }

    showPassword() {
        if (this.state.password.length < 1) {
            Alert.alert(
                'Warning',
                'Isi Password Dahulu',
                [
                    {
                        text: 'Oke', onPress: () => { null }
                    }],
                { cancelable: false }
            )
        } else {
            if (this.state.show == true) {
                this.setState({ show: false, fontPassword: 14, fontPassword2: 14 });
            } else {
                this.setState({ show: true, fontPassword: 30, fontPassword2: 35 });
            }
        }
    }
    showUlangiPassword() {
        if (this.state.ulangi_password.length < 1) {
            Alert.alert(
                'Warning',
                'Isi Password Dahulu',
                [
                    {
                        text: 'Oke', onPress: () => { null }
                    }],
                { cancelable: false }
            )
        } else {
            if (this.state.showUlangi == true) {
                this.setState({ showUlangi: false, fontPassword: 14, fontPassword2: 14 });
            } else {
                this.setState({ showUlangi: true, fontPassword: 30, fontPassword2: 35 });
            }
        }
    }
    SignUp() {
        let { username, fullname, password, gender, no_telp, ulangi_password, tempat_lahir, tanggal_lahir, alamat_domisili } = this.state
        tanggal_lahir = moment(tanggal_lahir).format("YYYY-MM-DD")
        if (password.length < 8) {
            this.root.message('Isi Password minimal 8.', 'warning')
        } else if (password != ulangi_password) {
            this.root.message('Ulangi Password harus sama.', 'warning')
        } else if (username.length != '' && fullname != '' && no_telp != '') {
            this.setState({ isLoading: true });
            Url.daftarCustomer(username, password, fullname, no_telp, alamat_domisili, gender, tempat_lahir, tanggal_lahir).then((response) => {
                let data = response.data
                this.setState({ isLoading: false });
                console.log('data daftar:', data);
                this.root.message(data.message, 'success')
                this.root.goBack()
            }).catch((error) => {
                console.log(error, error.response);
                if (error.response == null) {
                    this.root.message("Network Error.", 'warning')
                    this.setState({ isLoading: false });
                }
                this.root.message(error.response.data.message, 'warning');
                this.setState({ isLoading: false });
            });
        } else {
            this.root.message('Isi data diri dengan lengkap.', 'warning')
            this.setState({ page: 0 })
        }
    }
    getProfile(token) {
        Url.getProfile(token).then((response) => {
            let data = response.data.data;
            data.token = token
            this.setState({ isLoading: false, token });
            this.root.saveData(data)
            // console.log('data profile:', data);
        }).catch((error) => {
            // console.log(error, error.response);
            this.setState({ isLoading: false, });
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            this.root.message(error.response.data.message, 'warning');
        });
    }

    backHandle() {
        let { page } = this.state
        if (page == 0) {
            this.root.goBack()
        }
        else if (page == 1) {
            this.setState({ page: 0 })
        }
        else if (page == 2) {
            this.setState({ page: 1 })
        }
    }
    onDateChange = (event, date) => {
        // console.log(event, date)
        if (date === undefined) {
            // dismissedAction
            this.setState({ calendar: false })
        } else {
            this.setState({ tanggal_lahir: date, calendar: false })
        }
    }
    render() {
        let deviceId = this.root.state.deviceId
        let { image, imageKTP, imageKTPFace, defaultImage, ulangi_password, password, page } = this.state;
        return (
            <View style={[Styles.container]}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                <Header
                    backPress={() => this.backHandle()}
                    parent={this}
                    title="Daftar"
                />
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: Color.white }}
                    keyboardShouldPersistTaps='always'>

                    {page == 0 && (
                        <View style={{ flex: 1 }}>
                            <Image
                                source={require('../../../img/undraw_personal_info.png')}
                                style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 1.5, height: width / 2.5, marginVertical: 20 }]}
                            />
                            {/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity
                                    style={{ width: width / 2.5, height: width / 2.5, borderRadius: width / 2.5, backgroundColor: Color.grayWhite, justifyContent: 'center', alignItems: 'center', marginVertical: 10, alignSelf: 'center', borderColor: Color.primary, borderWidth: 1, }}
                                    onPress={this.state.ambilFoto == false ? () => this.ambilFoto() : null}>
                                    {this.state.ambilFoto == true && (
                                        <ActivityIndicator />
                                    )}
                                    {this.state.ambilFoto == false && (
                                        <Image
                                            resizeMode={"contain"}
                                            source={image != '' ? { uri: image } : defaultImage}
                                            style={[Styles.imageProfile, { borderRadius: width / 2.5 }]}
                                        />
                                    )}
                                </TouchableOpacity>


                                <Text style={Styles.primaryFontText}>Foto Profil</Text>
                            </View> */}
                            <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>
                                {/* <Text style={[Styles.primaryFontBold]}>Enter your fullname</Text> */}
                                <InputWithLabel
                                    // styleContainer={{ paddingLeft: 20 }}
                                    label="Username/Email :"
                                    labelColor={Color.black}
                                    placeholder="Username/Email"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={username => { this.setState({ username }) }}
                                    clearText={() => { this.setState({ username: '' }) }}
                                    // style={}
                                    defaultValue={this.state.username}
                                // underlineColorAndroid={}
                                />
                                <InputWithLabel
                                    // styleContainer={{ paddingLeft: 20 }}
                                    label="Nama Lengkap :"
                                    labelColor={Color.black}
                                    placeholder="Nama Lengkap"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={fullname => { this.setState({ fullname }) }}
                                    clearText={() => { this.setState({ fullname: '' }) }}
                                    // style={}
                                    defaultValue={this.state.fullname}
                                // underlineColorAndroid={}
                                />
                                <InputWithLabel
                                    // styleContainer={{ paddingLeft: 20 }}
                                    label="Alamat domisili :"
                                    labelColor={Color.black}
                                    placeholder="Alamat domisili"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={alamat_domisili => { this.setState({ alamat_domisili }) }}
                                    clearText={() => { this.setState({ alamat_domisili: '' }) }}
                                    // style={}
                                    defaultValue={this.state.alamat_domisili}
                                // underlineColorAndroid={}
                                />
                                <Text style={[Styles.primaryFontText, { fontSize: 12, marginHorizontal: 12 }]}>Jenis Kelamin :</Text>
                                <View style={[Styles.row, { paddingVertical: 8, borderBottomWidth: 1, borderBottomColor: Color.grayFill, marginVertical: 8 }]}>
                                    {this.state.dataGender.map((item, index) => {
                                        return (
                                            <TouchableOpacity key={index} style={[Styles.rowCenter, { justifyContent: 'flex-start', marginHorizontal: 10, marginVertical: 8 }]} onPress={() => this.setState({ gender: item.kode })}>
                                                <View style={styles.circle}>
                                                    {
                                                        this.state.gender == item.kode ?
                                                            < View style={{
                                                                height: 8,
                                                                width: 8,
                                                                borderRadius: 4,
                                                                backgroundColor: Color.primary,
                                                            }} />
                                                            : null
                                                    }
                                                </View>
                                                <Text style={[Styles.blackText, { fontSize: 12 }]}>{item.name}</Text>
                                            </TouchableOpacity>
                                        )
                                    })}
                                </View>
                                {/* <Text style={[Styles.primaryFontBold]}>Enter your username</Text> */}
                                <InputWithLabel
                                    // styleContainer={{ paddingLeft: 20 }}
                                    label="Phone Number :"
                                    labelColor={Color.black}
                                    placeholder="Phone Number"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={no_telp => { this.setState({ no_telp }) }}
                                    // style={}
                                    clearText={() => { this.setState({ no_telp: '' }) }}
                                    defaultValue={this.state.no_telp}
                                    keyboardType={"number-pad"}
                                // underlineColorAndroid={}
                                />
                                <InputWithLabel
                                    // styleContainer={{ paddingLeft: 20 }}
                                    label="Tempat Lahir :"
                                    labelColor={Color.black}
                                    placeholder="Surabaya, Malang, .. sesuai KTP"
                                    returnKeyType={"next"}
                                    // ref={(input) => this.password = input}
                                    // onSubmitEditing={() => this.password.focus()}
                                    onChangeText={tempat_lahir => { this.setState({ tempat_lahir }) }}
                                    clearText={() => { this.setState({ tempat_lahir: '' }) }}
                                    // style={}
                                    defaultValue={this.state.tempat_lahir}
                                // underlineColorAndroid={}
                                />
                                <InputWithLabel
                                    onPress={() => this.setState({ calendar: !this.state.calendar })}
                                    styleBox={{ marginVertical: 0 }}
                                    editable={false}
                                    label="Tanggal Lahir :"
                                    labelColor={Color.black}
                                    placeholder="Tanggal Lahir sesuai KTP"
                                    returnKeyType={"next"}
                                    defaultValue={moment(this.state.tanggal_lahir).format("dddd, DD MMMM YYYY")}
                                    iconRight={true}
                                    iconRightColor={Color.secondaryButton}
                                    iconRightName={'md-calendar'}
                                />
                                {this.state.calendar == true && (
                                    <RNDateTimePicker
                                        mode="date"
                                        minimumDate={new Date(1930, 0, 1)}
                                        maximumDate={new Date()}
                                        value={this.state.tanggal_lahir}
                                        onChange={this.onDateChange}
                                    />
                                )}
                            </View>
                        </View>
                    )}
                    {/* <Text style={[Styles.primaryFontBold]}>Enter your password</Text> */}
                    {page == 1 && (
                        <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>
                            <Text style={[Styles.primaryText, { marginVertical: 8 }]}>Gunakan password yang aman, dan mudah kamu ingat.</Text>
                            <InputWithLabel
                                // styleContainer={{ paddingLeft: 20 }}
                                label="Password :"
                                labelColor={Color.black}
                                placeholder="Password"
                                returnKeyType={"next"}
                                description={"Min. 8 characters"}
                                // ref={(input) => this.password = input}
                                // onSubmitEditing={() => this.password.focus()}
                                onChangeText={password => { this.setState({ password }) }}
                                clearText={() => { this.setState({ password: '' }) }}
                                // style={}
                                defaultValue={this.state.password}
                                // underlineColorAndroid={}
                                iconRight={this.state.password.length >= 1 ? true : false}
                                secureTextEntry={this.state.show}
                                iconRightName={this.state.show == true ? 'md-eye' : 'md-eye-off'}
                                iconPressed={() => { this.showPassword() }}
                            />

                            <InputWithLabel
                                // styleContainer={{ paddingLeft: 20 }}
                                label="Ulangi Password :"
                                labelColor={Color.black}
                                placeholder="Password"
                                returnKeyType={"next"}
                                description={password.length >= 8 && ulangi_password.length >= 8 ? password == ulangi_password ? "Siap, Password sudah sama" : "Ulangi Password belum sama" : ""}
                                // ref={(input) => this.password = input}
                                // onSubmitEditing={() => this.password.focus()}
                                onChangeText={ulangi_password => { this.setState({ ulangi_password }) }}
                                clearText={() => { this.setState({ ulangi_password: '' }) }}
                                // style={}
                                defaultValue={this.state.ulangi_password}
                                // underlineColorAndroid={}
                                iconRight={this.state.ulangi_password.length >= 1 ? true : false}
                                secureTextEntry={this.state.showUlangi}
                                iconRightName={this.state.showUlangi == true ? 'md-eye' : 'md-eye-off'}
                                iconPressed={() => { this.showUlangiPassword() }}
                            />
                        </View>
                    )}
                    {page == 2 && (
                        <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 20 }]}>
                            <Text style={Styles.primaryText}>Terakhir, masukan bukti identitas kamu.</Text>
                            <Text style={[Styles.primaryText, { fontSize: 10 }]}>Identitas kamu akan segera diverifikasi oleh admin.</Text>
                            <View style={{ width: '100%', height: 1, marginVertical: 20, borderColor: Color.gray, borderWidth: 1 }} />

                            <Text style={Styles.primaryFontText}>Foto KTP</Text>
                            <TouchableOpacity
                                style={{ width: width / 1.2, height: width / 1.7, borderRadius: 10, backgroundColor: Color.grayWhite, justifyContent: 'center', alignItems: 'center', marginVertical: 10, alignSelf: 'center', borderRadius: 10, borderColor: Color.primary, borderWidth: 1, }}
                                onPress={this.state.ambilFotoKTP == false ? () => this.ambilFotoKTP() : null}>
                                {this.state.ambilFotoKTP == true && (
                                    <ActivityIndicator />
                                )}
                                {this.state.ambilFotoKTP == false && (
                                    <Image
                                        resizeMode={"contain"}
                                        source={imageKTP != '' ? { uri: imageKTP } : defaultImage}
                                        style={[Styles.imageProfile, { borderRadius: 10 }]}
                                    />
                                )}
                            </TouchableOpacity>
                            <Text style={Styles.primaryFontText}>Foto KTP dengan wajah</Text>
                            <TouchableOpacity
                                style={{ width: width / 1.2, height: width / 1.7, borderRadius: 10, backgroundColor: Color.grayWhite, justifyContent: 'center', alignItems: 'center', marginVertical: 10, alignSelf: 'center', borderRadius: 10, borderColor: Color.primary, borderWidth: 1, }}
                                onPress={this.state.ambilFotoKTPFace == false ? () => this.ambilFotoKTPFace() : null}>
                                {this.state.ambilFotoKTPFace == true && (
                                    <ActivityIndicator />
                                )}
                                {this.state.ambilFotoKTPFace == false && (
                                    <Image
                                        resizeMode={"contain"}
                                        source={imageKTPFace != '' ? { uri: imageKTPFace } : defaultImage}
                                        style={[Styles.imageProfile, { borderRadius: 10 }]}
                                    />
                                )}
                            </TouchableOpacity>
                        </View>
                    )}
                    {this.state.isLoading == true && (
                        <ActivityIndicator />
                    )}
                    {/* <NoData iconName={"emoticon-tongue"}><Text>Pilih Ujian</Text></NoData> */}
                    {this.state.isLoading == false && (
                        <View style={[Styles.content, { flex: 1, paddingHorizontal: 20, paddingVertical: 4 }]}>
                            <Button
                                labelButton={page == 0 ? "Selanjutnya" : "Daftar Sekarang"}
                                onPress={page == 0 ? () => this.setState({ page: 1 }) : () => this.SignUp()}
                                backgroundColor={Color.secondaryButton}
                            // iconLeft={true}
                            // iconLeftName="md-eye"
                            />
                        </View>
                    )}
                </ScrollView>

                {/* {this.state.modalIntro == true && (
                    <ModalIntro
                        setStorage={() => AsyncStorage.setItem('SignUpIntro', 'sudah')}
                        data={this.state.dataIntro}
                    />
                )} */}
            </View >
        )
    }
}

const styles = StyleSheet.create({
    circle: {
        height: 16,
        width: 16,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: Color.grayDarkFill,
        alignItems: 'center',
        justifyContent: 'center', marginRight: 5
    }
})

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SignUp);