import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, Modal, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import InboxContent from '../component/InboxContent';
import LoadingImage from '../component/LoadingImage';
import NoData from '../component/NoData';
import Header from '../component/Header';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url.js';

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATTITUDE_DELTA = 0.004757
const LONGTITUDE_DELTA = LATTITUDE_DELTA * ASPECT_RATIO

class Notifikasi extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
        this.user = this.props.component.user;
        // this.userData = this.user.user;
        this.state = {
            isLoading: false,
            modalLoading: false,
            inbox: []
        }
    }
    _refresh() {
        this.setState({ isLoading: true });
        this.getNotifikasi()
    }
    componentDidMount() {
        // this.setState({ isLoading: true })

        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            // this.getNotifikasi()
        });
    }
    componentWillUnmount() {
        this.focusListener.remove();
    }
    getNotifikasi() {
        Url.getNotifikasi(this.userData.token).then((response) => {
            let data = response.data.data
            // console.log('data get notifikasi:', data);
            this.setState({ isLoading: false, inbox: data != null ? data.inbox : [] });
        }).catch((error) => {
            // console.log(error, error.response);
            if (error.response == null) {
                this.root.message("Network Error.", 'warning')
                this.setState({ isLoading: false });
            }
            // this.root.message(error.response.data.message, 'warning');
            this.setState({ isLoading: false });
        });
    }
    placeholder() {
        let content = [{}, {}, {}, {}, {}, {}]
        return (
            <LoadingImage />//Loading
        )
    }
    modalLoading() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalLoading}
                onRequestClose={() => null}>
                <TouchableOpacity style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)' }, Styles.center]}>
                    <Text style={Styles.whiteTextBold}>Sedang mengambil data..</Text>
                    <ActivityIndicator color={Color.white} size={"large"} />
                </TouchableOpacity>
            </Modal>
        )
    }


    render() {
        return (
            <View style={Styles.container}>
                <Header
                    parent={this}
                // title={"Feedback"}
                />
                {this.state.isLoading == true && (
                    this.placeholder()
                )}
                {this.state.isLoading == false && (
                    <ScrollView
                        style={{ flex: 1, marginVertical: 8 }}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        keyboardShouldPersistTaps='always'
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.isLoading}
                                onRefresh={this._refresh.bind(this)}
                            />
                        }>
                        <View style={Styles.content}>
                            <Text style={Styles.blackTextBold}>Notifikasi</Text>
                            {this.state.inbox.length == 0 && (
                                <NoData><Text>Belum ada Notifikasi</Text></NoData>
                            )}
                            {this.state.inbox.length > 0 && (
                                <InboxContent
                                    data={this.state.inbox}
                                />
                            )}
                        </View>
                        {/*<View style={{ height: 60 }} /> */}
                    </ScrollView>
                )}
                {this.modalLoading()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textInput: {
        flex: 1, height: 50, fontSize: 13, borderWidth: 1, borderColor: Color.blackOpacity,
        backgroundColor: Color.white, paddingHorizontal: 10,
        marginBottom: 5, borderRadius: 30
        // elevation: 3
    },
    picker: {
        width: '100%', height: 50,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFontFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
})
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
const ThisScreen = connect(
    mapStateToProps
)(Notifikasi);
export default withNavigation(ThisScreen);
