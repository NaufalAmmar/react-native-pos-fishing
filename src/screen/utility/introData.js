export default {
    login: [
        {
            step: 1,
            height: 200,
            topPosition: 200,
            description: "Jika kamu sudah punya akun, silahkan masuk disini. "
        },
        {
            step: 2,
            height: 50,
            topPosition: 440,
            description: "Jika kamu belum punya akun, silahkan daftar disini. "
        },
        {
            step: 3,
            height: 50,
            topPosition: 520,
            description: "atau kamu juga bisa menggunakan Facebook dan Google untuk melanjutkan. "
        },
    ],
    dashboard: [
        {
            step: 1,
            height: 150,
            topPosition: 200,
            description: "Disini kamu bisa lihat berita Cukurin Education terbaru. "
        },
        {
            step: 2,
            height: 55,
            topPosition: 430,
            description: "Jika mau Top Up atau Berlangganan klik disini. "
        },
        {
            step: 3,
            height: 60,
            topPosition: 500,
            description: "Pilih Kategori ujian ZENITO."
        },
        {
            step: 4,
            height: 60,
            topPosition: 570,
            description: "Pilih instansi ujian, disini adalah pilihan setelah kamu pilih kategori ujian 'ZENITO' diatas. "
        },
        {
            step: 5,
            height: 80,
            topPosition: 630,
            description: "Pilih Paket yang kamu inginkan. "
        },
        // {
        //     step: 6,
        //     height: 60,
        //     topPosition: 700,
        //     description: "Cek Rasionalisasi nilai kamu disini. "
        // },
    ]
};