import { Alert, } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob'
import Url from './Url.js';

export default {
    upload(type, username) {
        return new Promise((resolve, reject) => {
            let options = {
                title: 'Pilih Foto',
                storageOptions: {
                    skipBackup: true,
                    path: 'images'
                }
            };

            ImagePicker.showImagePicker(options, (response) => {
                if (response.didCancel) {
                    // console.log('User cancelled image picker');
                    reject({
                        message: 'Anda membatalkan pilih gambar.',
                    })
                }
                else if (response.error) {
                    // console.log('ImagePicker Error: ', response.error);
                    reject({
                        message: 'ImagePicker Error'
                    })
                }
                else if (response.customButton) {
                    // console.log('User tapped custom button: ', response.customButton);
                    reject({
                        message: 'Anda menekan tombol khusus'
                    })
                }
                else {
                    // console.log(response);
                    const url = Url.base_url + '/umum/file/upload-photo';

                    ImageResizer.createResizedImage(response.path, 1280, 1280, 'JPEG', 80, 0, null).then((response) => {
                        // response.uri is the URI of the new image that can now be displayed, uploaded...
                        // response.path is the path of the new image
                        // response.name is the name of the new image with the extension
                        // response.size is the size of the new image
                        // console.log('Hasil', response);

                        // console.log('Hasil', response);
                        RNFetchBlob.fetch('POST', url, {
                            'Content-Type': 'multipart/form-data',
                        }, [
                            { name: 'gambar', filename: type + username + moment().format("DD_MM_YYYY_HH:mm:ss") + '.jpeg', type: 'image/jpeg', data: RNFetchBlob.wrap(response.path) },
                        ]).then((response) => {
                            let data = JSON.parse(response.data);
                            // console.log('response upload gambar', data);
                            let hasil = data.data;
                            resolve({
                                status: "SUCCESS",
                                data: hasil.path
                            });

                        }).catch((err) => {
                            // console.log('Upload Error', err);

                            reject({
                                message: 'Foto Gagal diupload'
                            })
                        })
                    }).catch((err) => {
                        // console.log(err);
                        //Alert.alert('Informasi', 'Foto Gagal diresize.');
                        // Oops, something went wrong. Check that the filename is correct and
                        // inspect err to get more details.
                        Alert.alert("Information", err.response.data.data);
                        reject({
                            message: 'Foto Gagal diresize.',
                            data: err
                        })
                    });
                }
            });
        });
    }
}