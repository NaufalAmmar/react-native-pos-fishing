import React, { Component } from 'react';
import { ActivityIndicator, StatusBar, StyleSheet, View } from 'react-native';
import FitImage from 'react-native-fit-image';
import { connect } from 'react-redux';
import Color from '../utility/Color';

class SplashScreen extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Color.white }}>
                <StatusBar
                    backgroundColor={Color.primary}
                    barStyle="light-content"
                    style={{ opacity: 0.9 }}
                />
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    {/* <FitImage
                        originalWidth={100}
                        originalHeight={200}
                        source={require('../../../img/wifehouse.png')}
                        resizeMode='contain'
                        style={styles.image}
                    /> */}
                    <View style={{ flex: 1, height: 300, }}>
                        <FitImage
                            originalWidth={100}
                            originalHeight={50}
                            source={require('../../../img/logo.png')}
                            resizeMode='contain'
                            style={styles.imageLogo}
                        />
                    </View>
                </View>
                <View style={{ flex: 1, position: 'absolute', bottom: 10, alignSelf: 'center' }}>
                    <ActivityIndicator />
                    {/* <Text style={[Styles.bigBlackTextBold, { fontSize: 15, textAlign: 'center' }]}>Version 1.1 </Text> */}
                    {/* <Text style={[Styles.blackTextBold, { fontSize: 13, textAlign: 'center' }]}>Wash better clean with us!</Text> */}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create(
    {
        imageLogo: {
            width: 150, height: 50, alignSelf: 'center',

        }
    }
)

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SplashScreen);