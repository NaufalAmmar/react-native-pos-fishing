import moment from 'moment';
import React, { Component } from 'react';
import { Dimensions, Text, View } from 'react-native';
import FitImage from 'react-native-fit-image';
import Lightbox from 'react-native-lightbox';
import HTML from 'react-native-render-html';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Button from './Button';
const { width, height } = Dimensions.get('window')
class NewsContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    back() {
        this.root.goBack()
        this.root.navigate('ListNews')
    }
    render() {
        let str = this.props.description
        let html = str != null ? str.replace(`display: block;`, `display:flex;`) : ``;
        // console.log(html)
        return (
            <View style={Styles.container}>
                <Lightbox underlayColor="white">
                    <FitImage
                        resizeMode='contain'
                        style={styles.image}
                        source={this.props.image}
                    />
                </Lightbox>
                <View style={[Styles.content, { paddingVertical: 10 }]}>
                    <Text style={[Styles.blackTextBold, { color: Color.primary, fontSize: 16 }]}>{this.props.title}</Text>
                    <Text style={[Styles.blackText, { fontSize: 10 }]}>{moment(this.props.date).format("DD MMMM YYYY")}</Text>
                    <HTML
                        tagsStyles={{ p: { fontSize: 13, color: Color.black }, img: { display: 'flex', marginRight: 20, marginLeft: 20 } }}
                        html={html}
                        ignoredStyles={['display']}
                    />
                    <Button
                        radiusContainer={5}
                        backgroundColor={Color.secondaryButton}
                        onPress={() => this.back()}
                        iconLeft={true}
                        iconLeftName="ios-arrow-back"
                        labelButton="Kembali ke Daftar Artikel"
                    />
                </View>
            </View>
        )
    }
}
const styles = {
    wrapper: {
        // backgroundColor: Color.black,
        // borderBottomColor: Color.primary, borderBottomWidth: 1
    },
    slide: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: Color.greenFill
    },
    frontText: {
        width: '100%', backgroundColor: Color.blackOpacity,
        padding: 10, position: 'absolute', bottom: 0
    },

    image: {
        width: width, height: width / 2.5,
        // borderWidth:1, borderColor:Color.primary
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(NewsContent);