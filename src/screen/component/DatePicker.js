import moment from 'moment';
import React, { Component } from 'react';
import { DatePickerAndroid, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const dateNow = new Date();

class DatePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: ''
        }
        this.root = this.props.component.root;
    }
    async date() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                date: this.props.dateChoosed != null ? this.props.dateChoosed : new Date(),
                maxDate: this.props.maxDateChoosed != null ? this.props.maxDateChoosed : new Date(),
                minDate: this.props.minDateChoosed != null ? this.props.minDateChoosed : moment('20.07.1930 09:19', moment.defaultFormat).toDate()
            });
            if (action != DatePickerAndroid.dismissedAction) {
                let date = new Date(year, month, day);
                this.setState({
                    date: moment(date).format("YYYY-MM-DD")
                });
                this.props.onChangeDate(moment(date).format("YYYY MM DD"))
            }
        } catch ({ code, message }) {
            // console.log(message)
        }
    }
    render() {
        return (
            <View style={{ flex: 1, marginBottom: 10 }}>
                {/* <Text style={[Styles.blackText, { marginBottom: 5 }]}>{this.props.label}</Text> */}
                <TouchableOpacity style={styles.textInput} onPress={this.props.onPress != null ? this.props.onPress : () => this.date()}>
                    <TextInput
                        editable={false}
                        ref={(input) => this.props.ref = input}
                        placeholderTextColor="#ddd"
                        placeholder={this.props.placeholder}
                        // onChangeText={text => { this.props.onChangeText(text) }}
                        style={[{ flex: 1, color: Color.black }, this.props.styleTextInput]}
                        value={this.state.date}
                        underlineColorAndroid={this.props.underlineColorAndroid}
                    />
                    <TouchableOpacity activeOpacity={0.8} style={styles.iconShow} onPress={() => this.date()}>
                        <Ionicons name={"md-calendar"}
                            color={Color.primary}
                            size={25}
                        />
                    </TouchableOpacity>
                    {this.props.label != null && (
                        <Text style={[Styles.blackText, { fontSize: 12, position: 'absolute', top: -10, backgroundColor: Color.white, marginBottom: 10, marginLeft: 10, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                    )}
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = {
    textInput: {
        flex: 1, flexDirection: 'row',
        fontSize: 15, borderColor: Color.grayFill, borderWidth: 1, borderRadius: 2,
        backgroundColor: Color.white, paddingHorizontal: 10,
        alignItems: 'center'
    },
    iconShow: {
        width: 30, height: 30,
        justifyContent: 'center', alignItems: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(DatePicker);