import React, { Component } from 'react';
import { ActivityIndicator, Animated, Easing, Modal, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import NoData from '../component/NoData';
import Color from '../utility/Color';
import Styles from '../utility/Style';

const KEYS_TO_FILTERS = ['name'];

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0;
    return layoutMeasurement.height + contentOffset.y + 20 >=
        contentSize.height - paddingToBottom;
};
class SearchPicker extends Component {
    constructor(props) {
        super(props)
        this.animatedValue = new Animated.Value(0)
        this.state = {
            show: false,
            searchText: '',
            selected: '',
            selectedData: null,
            y: '',
            scrollEnd: '',
            event: null,
            text: '',
            searching: false,
            loadLoading: false
        }
    }
    searchText(input) {
        this.setState({ searchText: input })
        if (input.length == 0) {
            if (this.props.search != null) {
                this.props.search(input)
            }
            this.setState({ searching: false })
        }
    }
    close() {
        this.setState({ show: false, searching: false });
        if (this.state.searching == true) {
            this.props.search('')
            this.setState({ searching: false })
        }
    }
    searchSubmit() {
        this.props.search(this.state.text)
        this.setState({ searching: true })
    }
    componentDidMount() {

    }
    selected(item) {
        this.props.onChange(item.id, item.name, item);
        this.setState({ selectedData: item.id, selected: item.name, show: false, searchText: '' })
    }
    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 2000,
                useNativeDriver: true, 
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }
    refreshScroll(event) {
        this.setState({ y: event.nativeEvent.contentOffset.y, scrollEnd: false, event })
    }
    onScrollEnd() {
        let { y, scrollEnd, event, limit } = this.state
        this.setState({ scrollEnd: true })
        this.animate.bind(this)
        if (y == event.nativeEvent.contentOffset.y && scrollEnd == true) {
            this.setState({ limit: limit + 10 })
            alert("scroll end")
        }
    }
    load() {
        this.props.loadMore()
        this.setState({ loadLoading: true })
        setTimeout(() => {
            this.setState({ loadLoading: false })
        }, 300)
    }
    render() {
        let data = this.props.data
        const filteredData = data.filter(createFilter(this.state.searchText, KEYS_TO_FILTERS))
        let { searchText } = this.state
        return (
            <View style={[this.props.styleContainer, { flex: 1, marginVertical: 5, elevation: 3 }]}>

                <TouchableOpacity disabled={this.props.disabled != null ? this.props.disabled : false} style={[styles.button, this.props.styleButton]} onPress={() => {
                    this.setState({ show: true })
                }}>
                    {this.props.iconLeftName != null && (
                        <MaterialCommunityIcons name={this.props.iconLeftName}
                            color={this.props.iconLeftColor != null ? this.props.iconLeftColor : Color.white}
                            size={this.props.iconLeftSize}
                            style={{ marginRight: 5 }}
                        />
                    )}
                    <Text style={[styles.buttonText, { color: this.props.colorText != null ? this.props.colorText : Color.white }]} numberOfLines={1} ellipsizeMode='tail'>{this.props.selectedData == '' || this.props.selectedData == 0 || this.state.selected == '' ? this.props.dataTitle : this.state.selected}</Text>
                    <Ionicons name='md-arrow-dropdown' size={20} color={Color.white} />
                </TouchableOpacity>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.props.show != null ? this.props.show : this.state.show}
                    onRequestClose={() => {
                        this.close()
                    }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => { this.close() }}
                        style={[{ flex: 1, backgroundColor: Color.blackOpacity }]}
                    >
                        <View style={{ flex: 1, paddingHorizontal: 16, paddingVertical: 3, backgroundColor: Color.white, marginTop: 56 }}>
                            {/* <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => {
                                this.setState({ show: false });
                            }} /> */}
                            <TouchableOpacity onPress={this.props.onPressClose != null ? this.props.onPressClose : () => {
                                this.close()
                            }}>
                                <Text style={styles.close} > Close </Text>
                            </TouchableOpacity>
                            <View>
                                <View style={styles.search}>
                                    <MaterialIcons
                                        name="search"
                                        color="gray"
                                        size={24}
                                    />
                                    <View style={styles.searchInput}>
                                        <SearchInput
                                            onSubmitEditing={() => this.searchSubmit()}
                                            onChangeText={(input) => { this.searchText(input) }}
                                            placeholder="Cari"
                                        // clearIconViewStyles={{ position: 'absolute', right: 10, top: 10 }}
                                        // clearIcon={this.state.text !== '' && <MaterialIcons name="close" size={20} />}
                                        />
                                    </View>
                                </View>
                            </View>
                            <Text style={[Styles.primaryFontBold, { marginVertical: 3 }]} > Pilih Satu </Text>

                            <ScrollView style={{ paddingVertical: 10 }}
                                showsVerticalScrollIndicator={false}
                                onScroll={({ nativeEvent }) => {
                                    if (isCloseToBottom(nativeEvent) && searchText.length == 0) {
                                        this.load()
                                    }
                                }}
                            >


                                {filteredData.length == 0 && (
                                    <NoData><Text>Data Tidak ada </Text></NoData>
                                )}
                                {filteredData.map((item, index) => {
                                    let icon = this.state.selectedData == item.id ? 'md-square' : 'md-square-outline';
                                    return (
                                        <TouchableOpacity style={styles.item} key={index} onPress={() => { this.selected(item) }}>
                                            <Ionicons name={icon} size={30} color={Color.primary} />
                                            <Text style={styles.itemText}>{item.name}</Text>
                                        </TouchableOpacity>
                                    );
                                })}
                                {this.state.loadLoading == true && (
                                    <ActivityIndicator color={Color.primary} />
                                )}
                                <View style={{ height: 50 }} />
                            </ScrollView>
                            {/* {data != null && data.length != 0 && (
                                <TouchableOpacity onPress={this.state.text != '' ? () =>
                                    this.searchSubmit() : null}>
                                    <Text style={styles.selesai} > Cari </Text>
                                </TouchableOpacity>
                            )} */}
                        </View>
                    </TouchableOpacity>
                </Modal>
                {this.props.label != null && (
                    <Text style={[Styles.blackText, { fontSize: 12, position: 'absolute', top: -10, backgroundColor: Color.white, marginBottom: 10, marginLeft: 10, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                )}
            </View>
        );
    }
}

const styles = {
    search: {
        width: '100%', flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        alignSelf: 'center',
        borderWidth: 1, borderColor: Color.grayFill,
        marginHorizontal: 16, marginVertical: 10,
        paddingLeft: 5
    },
    searchInput: {
        flex: 1,
        padding: 5,
        color: Color.black,
    },
    button: {
        backgroundColor: Color.whiteOpacity,
        borderWidth: 1,
        borderColor: Color.grayFill,
        borderRadius: 3,
        paddingHorizontal: 20,
        height: 50,
        // marginBottom: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonText: {
        color: Color.white,
        fontSize: 14,
        flex: 1
    },
    modalRoot: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        width: 30,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    modalContent: {
        // position: 'absolute', left: 30, right: 30,
        // backgroundColor: '#fff',
        // borderRadius: 8,
        padding: 10,
        // flex: 1,
        // marginBottom: 0,
        // marginHorizontal: 30
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 2
    },
    close: {
        textAlign: 'right',
        marginVertical: 8
    },
    itemText: {
        flex: 1,
        color: '#000',
        marginLeft: 10,
        fontSize: 15
    },
    selesai: {
        // position: 'absolute', bottom: 10, right: 50, left: 50,
        marginHorizontal: 20,
        marginBottom: 10,
        color: Color.white,
        backgroundColor: Color.primary, padding: 15,
        borderRadius: 10,
        textAlign: 'center'
    },
};

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SearchPicker);