import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class IconContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <TouchableOpacity activeOpacity={0.8} style={[styles.box, { backgroundColor: this.props.backgroundColor != null ? this.props.backgroundColor : Color.white }]} onPress={this.props.onPress}>
                {this.props.iconLeftName != null && (
                    <MaterialIcons name={this.props.iconLeftName}
                        color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                        size={this.props.iconLeftSize}
                        style={{marginRight:5 }}
                    />
                )}
                <View style={Styles.rowBetween}>
                    <View>
                        <Text style={[Styles.whiteText, { fontSize: 10, textAlign: this.props.textAlign, color: this.props.nameTextColor != null ? this.props.nameTextColor : Color.white }]}>{this.props.name}</Text>
                        {this.props.otherText != null && (
                            <Text style={[Styles.whiteTextBold, { fontSize: 12, textAlign: this.props.textAlign, color: this.props.otherTextColor != null ? this.props.otherTextColor : Color.white }]}>{this.props.otherText}</Text>
                        )}
                        {this.props.childText != null && (
                            <Text style={[Styles.whiteText, { fontSize: 12, textAlign: this.props.textAlign, color: this.props.childTextColor != null ? this.props.childTextColor : Color.white }]}>{this.props.childText}</Text>
                        )}
                        {this.props.subChildText != null && (
                            <Text style={[Styles.whiteText, { flexWrap: "wrap", fontSize: 12, textAlign: this.props.textAlign, color: this.props.subChildTextColor != null ? this.props.subChildTextColor : Color.white }]}>{this.props.subChildText}</Text>
                        )}
                        {this.props.contact != null && (
                            <View style={styles.contact}>
                                <Ionicons name="md-person"
                                    color={Color.white}
                                    size={12}
                                    style={{ alignSelf: 'center', marginRight: 3 }}
                                />
                                <Text style={[Styles.whiteText, { fontSize: 12 }]}>{this.props.contact.substr(0, 7)}..</Text>
                            </View>
                        )}
                    </View>
                    {this.props.iconRight == true && (
                        <View style={styles.icon}>
                            {this.props.iconRightName != null && (
                                <Ionicons name={this.props.iconRightName}
                                    color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                    size={23}
                                    style={{ alignSelf: 'center' }}
                                />
                            )}
                            {this.props.materialIconRightName != null && (
                                <View style={[styles.circlePhoto, { backgroundColor: this.props.iconBackgroundColor != null ? this.props.iconBackgroundColor : null }]}>
                                    <MaterialIcons name={this.props.materialIconRightName}
                                        color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                        size={25}
                                        style={{ alignSelf: 'center' }}
                                    />
                                </View>
                            )}
                        </View>
                    )}
                    {this.props.textRight == true && (
                        <View style={styles.textRight}>
                            {this.props.textRightValue != null && (
                                <Text style={[Styles.whiteText, { color: this.props.textRightColor != null ? this.props.iconColor : Color.white, fontSize: 15 }]}>{this.props.textRightValue}</Text>
                            )}
                        </View>
                    )}
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = {
    box: {
        flex: 1, flexDirection: 'row',
        backgroundColor: Color.white,
        paddingVertical: 15,
        marginHorizontal:10,
        // marginBottom: 2,
        // borderColor: Color.whiteOpacity, borderBottomWidth: 1, borderTopWidth: 1,
        justifyContent: 'center', alignItems: 'center',
    },
    contact: {
        width: 70,
        flexDirection: 'row', borderRadius: 5,
        backgroundColor: Color.secondaryButton, padding: 2,
        marginVertical: 2,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 55, height: 55,
        borderRadius: 55, borderWidth: 1, borderColor: Color.gray,
        marginRight: 15,
        justifyContent: 'center', alignItems: 'center'
    },
    icon: {
        width: 30, height: 30, marginHorizontal: 5,
        justifyContent: 'center', alignItems: 'center', alignSelf: 'center'
    },
    textRight: {
        flex: 1,
        justifyContent: 'center', alignItems: 'flex-end', alignSelf: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(IconContent);