import React, { Component } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class InputWithLabel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[Styles.column, this.props.styleBox, { marginVertical: 8 }]}>
                <View style={[this.props.style, { flex: 1, borderBottomColor: Color.grayFill, borderBottomWidth: 1, borderRadius: 2 }]}>
                    <View style={[styles.textInput, this.props.styleContainer, { height: this.props.heightTextInput != null ? this.props.heightTextInput : this.props.multiline == true ? 30 + this.props.defaultValue.length : this.props.heightTextInput, }]}>
                        {this.props.labelLeft != null && (
                            <Text style={[Styles.blackText, { alignItems: 'center', fontSize: 15, color: this.props.labelLeftColor != null ? this.props.labelLeftColor : Color.primaryFont }]}>{this.props.labelLeft}</Text>
                        )}
                        {this.props.iconLeftName != null && (
                            <MaterialIcons name={this.props.iconLeftName}
                                color={this.props.iconLeftColor != null ? this.props.iconLeftColor : Color.white}
                                size={this.props.iconLeftSize}
                                style={{ marginRight: 5 }}
                            />
                        )}
                        <TextInput
                            maxLength={this.props.maxLength}
                            editable={this.props.editable}
                            ref={() => this.props.ref}
                            returnKeyType={this.props.returnKeyType}
                            onSubmitEditing={null}
                            placeholderTextColor={this.props.placeholderTextColor != null ? this.props.placeholderTextColor : "#ddd"}
                            placeholder={this.props.placeholder}
                            onChangeText={text => { this.props.onChangeText(text) }}
                            style={{ flex: 1, fontSize: 13, color: (this.props.editable == null || this.props.editable == true) ? this.props.inputColor != null ? this.props.inputColor : Color.primaryFont : Color.primary }}
                            defaultValue={this.props.defaultValue}
                            underlineColorAndroid={this.props.underlineColorAndroid}
                            secureTextEntry={this.props.secureTextEntry}
                            multiline={this.props.multiline}
                            keyboardType={this.props.keyboardType}
                        />
                        {this.props.clearText != null && this.props.defaultValue.length >= 1 && (this.props.editable == null || this.props.editable == true) && (
                            <TouchableOpacity activeOpacity={0.8} style={styles.iconShow} onPress={this.props.clearText}>
                                <Ionicons name={"md-close"}
                                    color={Color.primaryFont}
                                    size={20}
                                />
                            </TouchableOpacity>
                        )}
                        {this.props.iconRight == true && (
                            <TouchableOpacity activeOpacity={0.8} style={styles.iconShow} onPress={this.props.iconPressed}>
                                <Ionicons name={this.props.iconRightName}
                                    color={this.props.iconRightColor}
                                    size={20}
                                />
                            </TouchableOpacity>
                        )}
                        {this.props.buttonRight == true && (
                            <TouchableOpacity style={{ borderRadius: this.props.borderRadiusButton != null ? this.props.borderRadiusButton : 30, backgroundColor: this.props.backgroundColorButton != null ? this.props.backgroundColorButton : Color.secondaryButton, padding: 10, paddingVertical: 13, alignSelf: 'center', marginRight: 2, alignItems: 'center' }} activeOpacity={0.8} onPress={this.props.buttonRightPressed}>
                                <Text style={[Styles.blackText, { textAlign: 'center', color: this.props.buttonRightColor != null ? this.props.buttonRightColor : Color.black, }]}>{this.props.buttonRightLabel}</Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    {this.props.label != null && (
                        <Text style={[Styles.blackText, { fontSize: 12, position: 'absolute', top: -10, backgroundColor: Color.white, marginBottom: 10, marginLeft: 10, color: this.props.labelColor != null ? this.props.labelColor : Color.black }]}>{this.props.label}</Text>
                    )}


                </View >
                {this.props.description != null && (
                    <TouchableOpacity onPress={this.props.descriptionPressed}>
                        <Text style={[Styles.primaryText, { textAlign: 'right' }]}>{this.props.description}</Text>
                    </TouchableOpacity>
                )}
            </TouchableOpacity >
        )
    }
}
const styles = {
    textInput: {
        flex: 1, flexDirection: 'row', height: 45,
        borderRadius: 5, backgroundColor: Color.white, paddingHorizontal: 10,
        // borderColor: Color.grayFill, borderWidth: 1,
        justifyContent: 'center', alignItems: 'center'
    },
    iconShow: {
        width: 30, height: 30, marginRight: 0,
        justifyContent: 'center', alignItems: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(InputWithLabel);