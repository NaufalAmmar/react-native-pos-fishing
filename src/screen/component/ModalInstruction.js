import React, { Component } from 'react';
import { ActivityIndicator, CheckBox, Dimensions, Modal, ScrollView, StyleSheet, Text, View } from 'react-native';
import HTML from 'react-native-render-html';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
import Button from './Button';
const { width, height } = Dimensions.get('window')

class ModalInstruction extends Component {
    constructor(props) {
        super(props);
        this.user = this.props.component.user
        this.detailUjian = this.props.component.detail_ujian;
        this.instruksi = this.detailUjian.data_ujian.instruksi.content;
        this.state = {
            show: false,
            modalVisible: true,
            checked: false,
            instruksi: this.instruksi != null ?
                `<style>
            p, li {
                font-size: 10px;
            }
            </style>`
                + this.instruksi : '<p>Instruksi belum ada, Server error.</p>',
            checked: false
        };
    }
    action() {
        this.setState({ modalVisible: false })
        this.props.action();
    }
    close() {
        this.setState({ modalVisible: false })
    }
    componentDidMount() {
        // console.log('this.instruksi',this.instruksi)
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible != null ? this.props.visible : this.state.show}
                onRequestClose={() => {
                    this.props.onPressClose()
                }}
                style={{ flex: 1 }}
            >
                {/* <View style={{ position: 'absolute', top: 10, right: 10 }}>
                    <Button
                        labelButton="Tutup"
                        labelColor={Color.white}
                        onPress={() => this.props.onPressClose()}
                        backgroundColor={Color.redFill}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                </View> */}
                <View style={Styles.column}>
                    {/* <TouchableOpacity onPress={() => this.props.onPressClose()} style={[{ width: 35, backgroundColor: 'rgba(0,0,0,0.3)' }]} /> */}
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        style={{ backgroundColor: Color.white, flex: 1, padding: 15, backgroundColor: Color.white, paddingHorizontal: 20, paddingVertical: 5 }}>
                        {this.state.instruksi == '' && (
                            <ActivityIndicator size="large" color={Color.black} />
                        )}
                        
                            <Text style={[Styles.blackText, { color: Color.secondaryButton, fontSize: 20, fontFamily: 'sans-serif' }]}>Instruksi</Text>
                            <HTML ignoredStyles={['display']} tagsStyles={{ p: { fontSize: 13, width: width / 1.5 }, li: { fontSize: 12, width: width / 1.5 }, }} style={{ fontSize: 10, fontWeight: 'bold', color: Color.black }} html={this.state.instruksi} />
                            <View style={[Styles.rowCenter, { flex: 0, justifyContent: 'flex-start' }]}>
                                <CheckBox
                                    value={this.state.checked}
                                    onValueChange={() => this.setState({ checked: !this.state.checked })}
                                />
                                <Text style={Styles.blackText}>Smartphone yang saya gunakan dapat bekerja dengan baik.</Text>
                            </View>
                            <Button
                                labelButton="Mulai Tryout"
                                labelColor={Color.white}
                                onPress={() => this.state.checked == true ? this.props.onPress() : null}
                                backgroundColor={this.state.checked == true ? Color.secondaryButton : Color.grayFill}
                            // iconLeft={true}
                            // iconLeftName="md-eye"
                            />
                        {/* <View style={{ margin: 20 }} /> */}
                    </ScrollView>
                    {/* <TouchableOpacity onPress={() => this.props.onPressClose()} style={[{ width: 35, backgroundColor: 'rgba(0,0,0,0.3)' }]} /> */}
                </View>
            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        alignItems: 'center', justifyContent: 'center'
    },
    modal: {
        width: 300,
        paddingHorizontal: 5, paddingVertical: 5,
        backgroundColor: Color.white,
        padding: 5,
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 17, fontWeight: 'bold',
        color: '#000',
        borderBottomWidth: 1,
        borderColor: Color.grayFill,
        marginTop: 10
    },
    text: {
        fontSize: 16,
        color: Color.black,
        textAlign: 'center'
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        position: 'absolute',
        bottom: 15, right: 15
    },
    textAction: {
        fontSize: 15,
        color: Color.white,
    },
    textClose: {
        fontSize: 15,
        // backgroundColor: Color.redFill,
        color: Color.black,
        padding: 5,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalInstruction);