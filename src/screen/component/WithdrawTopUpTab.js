import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../utility/Color.js';
import TopUp from '../user/TopUp';
import Withdraw from '../user/Withdraw';
import Styles from '../utility/Style.js';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
class WithdrawScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <Withdraw />
            </View>
        );
    }
}

class TopUpScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <TopUp />
            </View>
        );
    }
}


export default createAppContainer(createMaterialTopTabNavigator({
    Withdraw: { screen: WithdrawScreen, },
    "Top Up": { screen: TopUpScreen, },
},

    // { initialRouteName: 'Aktivitas' },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;

            },

        }),
        tabBarOptions: {
            // showIcon: true,
            keyboardHidesTabBar: true,
            activeTintColor: Color.primary,
            inactiveTintColor: Color.blackOpacity,
            upperCaseLabel: false,
            indicatorStyle: {
                backgroundColor: Color.primary
            },
            labelStyle: {
                fontSize: 16,
            },
            tabStyle: {
                flex: 1, height: 60,
            },
            style: {
                backgroundColor: 'white',
            },
        },

    }
));