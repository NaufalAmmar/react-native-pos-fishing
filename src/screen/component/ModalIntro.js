import React, { Component } from 'react';
import { Dimensions, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
const { width, height } = Dimensions.get('window')

class ModalIntro extends Component {
    constructor(props) {
        super(props);
        this.user = this.props.component.user

        this.state = {
            show: true,
            modalVisible: true,
            step: 1,
            top: 0,
        };
    }

    componentDidMount() {
        let data = this.props.data;
        if (data != null) {
            this.setState({ top: parseInt(data[0].topPosition) + parseInt(data[0].height) })
            // alert(parseInt(data[0].topPosition) + parseInt(data[0].height))
        }
    }
    onPress(step) {
        let { top } = this.state
        let data = this.props.data;
        if (data != null) {
            if (step == this.props.data.length) {
                this.props.setStorage();
            } else {
                this.setState({ top: parseInt(data[step].topPosition) + parseInt(data[step].height) })
            }
            // alert(parseInt(data[0].topPosition) + parseInt(data[0].height))
        }
        this.setState({ step: this.state.step + 1 })
        if (step == this.props.data.length) {
            this.props.setStorage();
        }
    }
    setStorage() {
        this.props.setStorage();
        this.setState({ step: this.props.data.length })
    }
    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.step >= this.props.data.length ? false : true}
                onRequestClose={null} >
                <TouchableOpacity onPress={() => null} style={[{ flex: 1 }]}>
                    <TouchableOpacity onPress={() => this.onPress(this.state.step)} style={{ flex: 1, top: this.state.top, backgroundColor: Color.blackOpacity }} />
                    {this.props.data.map((item, index) => {
                        if (index + 1 == this.state.step) {
                            return (
                                <View key={index} style={{ flex: 1, position: 'absolute', top: item.topPosition, }}>
                                    <TouchableOpacity onPress={() => this.onPress(item.step)}
                                        style={[{ width: width, height: item.height, borderRadius: 10, borderWidth: 1, borderColor: Color.black, }]}>
                                    </TouchableOpacity>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ padding: 5, borderRadius: 5, borderTopLeftRadius: 0, borderWidth: 1, borderColor: Color.primary, marginHorizontal: 20, marginRight: 200, color: Color.black, backgroundColor: Color.white }}>{item.description}</Text>
                                    </View>
                                    <View style={Styles.rowBetween}>
                                        <TouchableOpacity onPress={() => this.setStorage()} style={{ marginVertical: 10 }}>
                                            <Text style={{ padding: 5, borderRadius: 10, borderTopRightRadius: 0, marginHorizontal: 20, color: Color.white, backgroundColor: Color.primary }}>Skip</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.onPress(item.step)} style={{ marginVertical: 10 }}>
                                            <Text style={{ padding: 5, borderRadius: 10, borderTopLeftRadius: 0, marginHorizontal: 20, color: Color.white, backgroundColor: Color.secondaryButton }}>Next</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }
                    })}
                </TouchableOpacity>
            </Modal >
        )
    }
}
const styles = StyleSheet.create({

});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalIntro);