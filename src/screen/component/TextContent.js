import React, { Component } from 'react';
import { Modal, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import NoData from '../component/NoData';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Button from './Button.js';

class TextContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clicked: 0,
            modalDetail: false,
            data_detail: null
        }
        this.root = this.props.component.root;
    }
    modalDetail() {
        let { data_detail } = this.state
        // console.log('data_detail', data_detail);
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={true}
                // onRequestClose={() => this.setState({ modalVisible: false })}>
                onRequestClose={() => null}>
                <View style={[{ flex: 1, backgroundColor: 'rgba(0,0,0,0.3)', paddingHorizontal: 20 }]}>
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        style={{ marginVertical: 5 }}
                        style={{ flex: 1, backgroundColor: Color.white, paddingHorizontal: 20, paddingVertical: 20 }}
                    >
                        <Text style={[Styles.blackTextBold, { fontSize: 15, marginBottom: 10 }]}>Kunjungan</Text>
                        {data_detail.data_kunjungan.length != 0 && (
                            data_detail.data_kunjungan.map((item, index) => {
                                return (
                                    <View key={index} style={styles.box}>
                                        <View style={Styles.rowBetween}>
                                            <Text style={[Styles.blackTextBold, { fontSize: 14 }]}>{item.type}</Text>
                                            <Text style={[Styles.blackText, { fontSize: 14 }]}>{item.created_at}</Text>
                                        </View>
                                        <Text style={[Styles.grayText, { fontSize: 13 }]}>{item.member_name}</Text>
                                    </View>
                                )
                            })
                        )}
                        {data_detail.data_kunjungan.length == 0 && (
                            <NoData><Text>Tidak ada Data</Text></NoData>

                        )}
                        <Text style={[Styles.blackTextBold, { fontSize: 15, marginBottom: 10, marginTop:20 }]}>User Baru</Text>
                        {data_detail.data_user.length != 0 && (
                        data_detail.data_user.map((item, index) => {
                            return (
                                <View key={index} style={styles.box}>
                                    <View style={Styles.rowBetween}>
                                        <Text style={[Styles.blackTextBold, { fontSize: 14 }]}>{item.type}</Text>
                                        <Text style={[Styles.blackText, { fontSize: 14 }]}>{item.created_at}</Text>
                                    </View>
                                    <Text style={[Styles.grayText, { fontSize: 13 }]}>{item.member_name}</Text>
                                </View>
                            )
                        })
                    )}
                        {data_detail.data_user.length == 0 && (
                        <NoData><Text>Tidak ada Data</Text></NoData>
                    )}
                        <Button
                            labelButton="Tutup"
                            backgroundColor={Color.primary}
                            onPress={() => this.setState({ modalDetail: false, data_kunjungan: null, data_user: null })}
                        />
                    </ScrollView>
                </View>
            </Modal>
        )
    }
    onClick(item) {
        // console.log(item)

        this.setState({ modalDetail: true, data_detail: item })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={Styles.rowBetween}>
                    <Text style={[Styles.blackTextBold, { fontSize: 15, marginBottom: 10 }]}>{this.props.title}</Text>
                    {this.props.data.length != 0 && (
                        <TouchableOpacity onPress={this.props.linkPress}>
                            <Text style={[Styles.blackTextBold, { fontSize: 15, marginBottom: 10, color: Color.secondaryButton }]}>{this.props.link}</Text>
                        </TouchableOpacity>
                    )}
                </View>
                <View style={styles.menu} >
                    {this.props.data.map((item, index) => {
                        return (
                            <TouchableOpacity onPress={item.data_kunjungan != null && item.data_kunjungan.length != 0 || item.data_user != null && item.data_user.length != 0 ? () => this.onClick(item) : null} key={index} style={styles.box}>
                                <View style={Styles.rowBetween}>
                                    <Text style={[Styles.blackTextBold, { fontSize: 14 }]}>{item.label}</Text>
                                    <Text style={[Styles.blackText, { fontSize: 14 }]}>{item.tanggal}</Text>
                                </View>
                                <Text style={[Styles.grayText, { fontSize: 13 }]}>{item.keterangan}</Text>
                            </TouchableOpacity>
                        )
                    })}
                    {this.props.data.length == 0 && (
                        <NoData><Text>Belum ada Data</Text></NoData>
                    )}
                </View>

                {this.state.modalDetail == true && (
                    this.modalDetail()
                )}
            </View >
        )
    }
}
const styles = {
    container: {
        marginVertical: 5
    },
    menu: {
        backgroundColor: Color.white,
        flex: 1, padding: 10,
        borderRadius: 5, elevation: 3
    },
    box: {
        backgroundColor: Color.white,
        paddingVertical: 5,
        marginHorizontal: 5,
        borderBottomWidth: 1, borderColor: Color.gray
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(TextContent);