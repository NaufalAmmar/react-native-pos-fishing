import React, { Component } from 'react';
import { ActivityIndicator, Modal, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import HTML from 'react-native-render-html';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
import Url from '../utility/Url';
import Button from './Button';

class ModalTNC extends Component {
    constructor(props) {
        super(props);
        this.user = this.props.component.user
        this.state = {
            modalVisible: true,
            checked: false,
            syarat: ''
        };
    }
    action() {
        this.setState({ modalVisible: false })
        this.props.action();
    }
    close() {
        this.setState({ modalVisible: false })
    }
    componentDidMount() {
        this.loadSyarat();
    }
    loadSyarat() {
        Url.getSyarat(0).then((response) => {
            let data = response.data.data;
            Object.keys(data).forEach((item) => {
                this.setState({ syarat: data[0].isi })
            })
            // console.log('data tnc:', data);
        }).catch((error) => {
            // console.log(error, error.response);
            this.setState({ isLoading: false });
        });
    }
    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => this.props.onPressClose()}>
                {/* <View style={{ position: 'absolute', top: 10, right: 10 }}>
                    <Button
                        labelButton="Tutup"
                        labelColor={Color.white}
                        onPress={() => this.props.onPressClose()}
                        backgroundColor={Color.redFill}
                    // iconLeft={true}
                    // iconLeftName="md-eye"
                    />
                </View> */}
                <View style={Styles.row}>
                    <TouchableOpacity onPress={() => this.props.onPressClose()} style={[{ width: 35, backgroundColor: 'rgba(0,0,0,0.3)' }]} />
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        style={{ backgroundColor: Color.white, flex: 1, padding: 15, }}>
                        <Text style={[Styles.blackTextBold, { fontSize: 20 }]}>Syarat dan Ketentuan</Text>
                        {this.state.syarat == '' && (
                            <ActivityIndicator size="large" color={Color.black} />
                        )}
                        <HTML ignoredStyles={['display']} style={{ fontSize: 20, fontWeight: 'bold', color: Color.black }} html={this.state.syarat} />
                        <Button
                            labelButton="Tutup"
                            labelColor={Color.white}
                            onPress={() => this.props.onPressClose()}
                            backgroundColor={Color.redFill}
                        // iconLeft={true}
                        // iconLeftName="md-eye"
                        />
                        <View style={{ margin: 20 }} />
                    </ScrollView>
                    <TouchableOpacity onPress={() => this.props.onPressClose()} style={[{ width: 35, backgroundColor: 'rgba(0,0,0,0.3)' }]} />
                </View>
            </Modal>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        alignItems: 'center', justifyContent: 'center'
    },
    modal: {
        width: 300,
        paddingHorizontal: 5, paddingVertical: 5,
        backgroundColor: Color.white,
        padding: 5,
    },
    button: {
        // flex: 1,
        backgroundColor: Color.primary,
        padding: 5,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 17, fontWeight: 'bold',
        color: '#000',
        borderBottomWidth: 1,
        borderColor: Color.grayFill,
        marginTop: 10
    },
    text: {
        fontSize: 16,
        color: Color.black,
        textAlign: 'center'
    },
    action: {
        marginVertical: 5, marginHorizontal: 10,
        flex: 1,
        // flexDirection: 'row',
        // justifyContent: 'space-between',
        position: 'absolute',
        bottom: 15, right: 15
    },
    textAction: {
        fontSize: 15,
        color: Color.white,
    },
    textClose: {
        fontSize: 15,
        // backgroundColor: Color.redFill,
        color: Color.black,
        padding: 5,
        textAlign: 'center'
    }
});
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ModalTNC);