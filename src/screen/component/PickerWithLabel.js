import React, { Component } from 'react';
import { ActivityIndicator, Animated, Easing, Modal, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

class PickerWithLabel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            selected: '',
            selectedData: null,
            event: null,
            loadLoading: false
        }
        this.root = this.props.component.root;
    }
    selected(item) {
        this.props.onChange(item.value, item.label, item);
        this.setState({ selectedData: item.id, selected: item.label, show: false, })
    }
    close() {
        this.setState({ show: false });
        if (this.state.searching == true) {
            this.setState({ searching: false })
        }
    }
    render() {
        return (
            <View style={{ flex: 1, marginTop: 5, marginBottom: 5, borderWidth: 1, borderColor: Color.gray, }}>
                <TouchableOpacity disabled={this.props.disabled != null ? this.props.disabled : false} style={[styles.button, this.props.styleButton]} onPress={() => {
                    this.setState({ show: true })
                }}>
                    {this.props.iconLeftName != null && (
                        <MaterialCommunityIcons name={this.props.iconLeftName}
                            color={this.props.iconLeftColor != null ? this.props.iconLeftColor : Color.white}
                            size={this.props.iconLeftSize}
                            style={{ marginRight: 5 }}
                        />
                    )}
                    <Text style={[styles.buttonText, { color: this.props.colorText != null ? this.props.colorText : Color.primaryFont }]} numberOfLines={1} ellipsizeMode='tail'>{this.props.selectedData == '' || this.props.selectedData == 0 || this.state.selected == '' ? this.props.labelPicker : this.state.selected}</Text>
                    <Ionicons name='md-arrow-dropdown' size={20} color={Color.primaryFont} />
                </TouchableOpacity>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.show}
                    onRequestClose={() => {
                        this.close()
                    }}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => {
                            this.close()
                        }} />

                        <View style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Color.white }}>
                            {/* <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => {
                                this.setState({ show: false });
                            }} /> */}
                            <ScrollView style={{ paddingVertical: 10 }}
                                onScroll={({ nativeEvent }) => {
                                    if (isCloseToBottom(nativeEvent) && searchText.length == 0) {
                                        this.load()
                                    }
                                }}
                            >

                                <TouchableOpacity onPress={this.props.onPressClose != null ? this.props.onPressClose : () => {
                                    this.close()
                                }}>
                                    <Text style={styles.close} > Close </Text>
                                </TouchableOpacity>
                                {this.props.dataPicker.length == 0 && (
                                    <NoData><Text>Data Tidak ada </Text></NoData>
                                )}
                                {this.props.dataPicker.map((item, index) => {
                                    let icon = this.state.selectedData == item.value ? 'md-checkmark-circle' : 'md-radio-button-off';
                                    return (
                                        <TouchableOpacity style={styles.item} key={index} onPress={() => { this.selected(item) }}>
                                            {/* <Ionicons name={icon} size={30} color={Color.primary} /> */}
                                            <Text style={styles.itemText}>{item.label}</Text>
                                        </TouchableOpacity>
                                    );
                                })}
                                {this.state.loadLoading == true && (
                                    <ActivityIndicator color={Color.primary} />
                                )}
                                <View style={{ height: 50 }} />
                            </ScrollView>
                        </View>
                        <TouchableOpacity style={styles.modalRoot} activeOpacity={1} onPress={() => {
                            this.close()
                        }} />
                    </View>
                </Modal>
                {this.props.label != null && (
                    <Text style={[Styles.bigPrimaryFontTextBold, { fontSize: 12, position: 'absolute', top: -10,marginLeft:10, backgroundColor: Color.white, color: this.props.labelPickerColor != null ? this.props.labelPickerColor : Color.primary, marginBottom: 5 }]}>{this.props.label}</Text>
                )}
            </View>
        )
    }
}
const styles = {
    textInput: {
        width: '100%', height: 40, fontSize: 13,

        backgroundColor: Color.white, paddingHorizontal: 10,
        elevation: 3
    },
    picker: {
        width: '100%', height: 40,
        //  fontSize: 13,
        // borderWidth: 1, borderColor: Color.gray,
        color: Color.primaryFont,
        // backgroundColor: Color.white,
        // elevation: 3
    },
    iconShow: {
        width: 30, height: 30,
        justifyContent: 'center', alignItems: 'center'
    },
    button: {
        backgroundColor: Color.whiteOpacity,
        borderWidth: 1,
        borderColor: Color.blackOpacity,
        borderRadius: 5,
        paddingHorizontal: 20,
        height: 50,
        // marginBottom: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonText: {
        color: Color.white,
        fontSize: 14,
        flex: 1
    },
    modalRoot: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        width: 30,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    modalContent: {
        // position: 'absolute', left: 30, right: 30,
        // backgroundColor: '#fff',
        // borderRadius: 8,
        padding: 10,
        // flex: 1,
        // marginBottom: 0,
        // marginHorizontal: 30
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 2
    },
    close: {
        textAlign: 'right'
    },
    itemText: {
        flex: 1,
        color: '#000',
        marginLeft: 10,
        paddingVertical:7,
        borderColor:Color.grayFill, borderBottomWidth:1,
        fontSize: 15
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(PickerWithLabel);