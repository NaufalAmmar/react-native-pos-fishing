import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width } = Dimensions.get('window')

class CircleButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <View style={[Styles.center, { marginHorizontal: 10 }]}>
                <TouchableOpacity disabled={this.props.disabled} activeOpacity={0.8} style={[styles.buttonPrimary, this.props.style, { width: this.props.circleSize, height: this.props.circleSize, borderRadius: this.props.circleSize, backgroundColor: this.props.backgroundColor == null ? Color.button : this.props.backgroundColor, borderRadius: this.props.borderRadius == null ? 30 : this.props.borderRadius, justifyContent: this.props.justifyContent != null ? this.props.justifyContent : 'center' }]} onPress={this.props.onPress}>
                    <View style={[styles.icon, { flex: 1, flexDirection: 'row', alignItems: this.props.justifyIconRight != null ? this.props.justifyIconRight : 'center' }]}>
                        {this.props.icon != null && (
                            <Ionicons name={this.props.icon}
                                color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                size={23}
                            />
                        )}
                        {this.props.sourceImage != null && (
                            <Image
                                // source={require('../../../img/background.jpg')}
                                source={this.props.sourceImage}
                                style={[styles.circlePhoto, { width: this.props.imageSize, height: this.props.imageSize, borderRadius: this.props.radiusImage != null ? this.props.radiusImage : this.props.imageSize }]}
                            />
                        )}
                    </View>
                </TouchableOpacity>
                {this.props.label != null && (
                    <Text style={[Styles.blackText, { fontSize: 12, color: this.props.labelColor != null ? this.props.labelColor : Color.primary }]}>{this.props.label.length >= Math.round(width / 40) && this.props.cutLabel == null ? this.props.label.substring(0, Math.round(width / 40)) + ".." : this.props.label}</Text>
                )}

                {this.props.rateUser != null && (
                    <View style={{ flex: 1, paddingHorizontal: 5, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', backgroundColor: this.props.rateTextBackColor, borderRadius: 10, }}>
                        <Ionicons name="ios-star"
                            color={Color.primary}
                            size={10}
                            style={{ alignSelf: 'center', marginRight: 3 }}
                        />
                        <Text style={[Styles.blackText, { fontSize: 10, color: this.props.rateUserColor != null ? this.props.rateUserColor : Color.primary }]}>{this.props.rateUser}</Text>
                    </View>
                )}
                {this.props.childLabel != null && (
                    <Text style={[Styles.blackText, { fontSize: 8, color: this.props.childLabelColor != null ? this.props.childLabelColor : Color.black }]}>{this.props.childLabel.length >= Math.round(width / 40) ? this.props.childLabel.substring(0, Math.round(width / 40)) + ".." : this.props.childLabel}</Text>
                )}
            </View>
        )
    }
}
const styles = {
    buttonPrimary: {
        flexDirection: 'row', backgroundColor: Color.button,
        // paddingHorizontal: 20,        
        justifyContent: 'center', alignItems: 'center',
        marginHorizontal: 3, marginBottom: 5,
        elevation: 3
    },
    icon: {
        width: 20, height: 18,
        justifyContent: 'center', alignItems: 'center'
    },
    circlePhoto: {
        width: 55, height: 55,
        borderRadius: 55, borderWidth: 1, borderColor: Color.gray,
        // marginRight: 15,
        alignSelf: 'center',
        justifyContent: 'center', alignItems: 'center'
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(CircleButton);