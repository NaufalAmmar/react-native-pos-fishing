import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, Dimensions, Modal } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { dispatch as Redux } from '@rematch/core';
const { width, height } = Dimensions.get('window')

class ScrollHorizontalImage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clicked: 0,
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                horizontal={true}>
                <View style={styles.menu} >
                    {this.props.data.map((item, ind) => {
                        if (ind < 5) {
                            return (
                                <TouchableOpacity activeOpacity={0.8} key={ind} style={[styles.box, { backgroundColor: Color.button }]} onPress={() => {
                                    this.props.onPress(item)
                                }}>
                                    <Image
                                        source={{ uri: item.imageUrl }}
                                        style={Styles.backgroundBerita}
                                    />
                                  
                                </TouchableOpacity>
                            )
                        }

                    })}
                </View>
            </ScrollView>
        )
    }
}
const styles = {
    menu: {
        flex: 1, flexDirection: 'row',
        paddingHorizontal: 24,
    },
    box: {
        width: width / 1.13,
        height: 135,
        flexDirection: 'row',
        // paddingVertical: 5,
        marginRight: 8,
        borderRadius: 8,
        justifyContent: 'center', alignItems: 'center'
    },
    linear: {
        width: '100%', height: '40%',
        paddingHorizontal: 10,
        borderBottomLeftRadius: 8, borderBottomRightRadius: 8,
        paddingVertical: 5, alignSelf: 'flex-end',
        backgroundColor: 'rgba(6,194,31,0.9)'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ScrollHorizontalImage);