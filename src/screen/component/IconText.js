import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class IconText extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <View style={[styles.container, { backgroundColor: this.props.backgroundColor == null ? 'transparent' : this.props.backgroundColor, justifyContent: this.props.justifyContent != null ? this.props.justifyContent : 'center' }]}>
                {this.props.iconLeft == true && (
                    <View style={styles.icon}>
                        {this.props.iconLeftName != null && (
                            <Ionicons name={this.props.iconLeftName}
                                color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                size={27}
                            />
                        )}
                        {this.props.materialIconLeftName != null && (
                            <MaterialIcons name={this.props.materialIconLeftName}
                                color={this.props.iconColor != null ? this.props.iconColor : Color.white}
                                size={27}
                            />
                        )}
                    </View>
                )}
                <Text style={[Styles.whiteText, { color: this.props.labelColor != null ? this.props.labelColor : Color.white }]}>{this.props.text}</Text>
                {this.props.iconRight == true && (
                    <View style={styles.icon}>
                        {this.props.iconRightName != null && (
                            <Ionicons name={this.props.iconRightName}
                                color={Color.white}
                                size={27}
                            />
                        )}
                        {this.props.materialIconRightName != null && (
                            <MaterialIcons name={this.props.materialIconRightName}
                                color={Color.white}
                                size={27}
                            />
                        )}
                    </View>
                )}
            </View>
        )
    }
}
const styles = {
    container: {
        flexDirection: 'row',
        height: 30,
        backgroundColor: 'transparent',
        justifyContent: 'center', alignItems: 'center', marginVertical: 4,
        // elevation: 3
    },
    icon: {
        width: 30, height: 30, marginRight: 10,
        justifyContent: 'center', alignItems: 'center'
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(IconText);