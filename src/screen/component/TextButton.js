import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Styles from '../utility/Style.js';

class TextButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    // showPassword() {
    //     if (this.state.password.length < 1) {
    //         Alert.alert(
    //             'Warning',
    //             'Isi Password Dahulu',
    //             [
    //                 {
    //                     text: 'Oke', onPress: () => { null }
    //                 }],
    //             { cancelable: false }
    //         )
    //     } else {
    //         if (this.state.show == true) {
    //             this.setState({ show: false, fontPassword: 14, fontPassword2: 14 });
    //         } else {
    //             this.setState({ show: true, fontPassword: 30, fontPassword2: 35 });
    //         }
    //     }
    // }
    render() {
        return (
            <View>
                {/* {this.state.isLoading == true && (
                    <ActivityIndicator size="large" color={Color.primary} />
                )} */}
                <TouchableOpacity activeOpacity={0.8} style={styles.buttonTransparent} onPress={this.props.onPress}>
                    <Text style={[Styles.blackText, { textDecorationLine: 'underline', fontSize: 13, textAlign: this.props.textAlign, color: this.props.color }]}>{this.props.labelButton}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = {
    buttonTransparent: {
        flex: 1, backgroundColor: 'transparent', marginVertical: 5
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(TextButton);