import React, { Component } from 'react';
import { Dimensions, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
const { width } = Dimensions.get('window')

class ListRingkasan extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
    }

    render() {
        return (
            <View style={[Styles.column, { borderTopColor: Color.gray, borderTopWidth: 1, backgroundColor: Color.white, paddingVertical: 5, marginVertical: 5 }]}>
                <Text style={[Styles.bigPrimaryFontTextBold, { marginTop: 5 }]}>{this.props.label}</Text>
                <View style={[Styles.rowBetween, { marginTop: 5, paddingVertical: 5, paddingHorizontal: 10, }]}>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Sesi</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Benar</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Salah</Text>
                    <Text style={[Styles.primaryTextBold, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>Kosong</Text>
                </View>
                {this.props.data.map((item, index) => {
                    return (
                        <View key={index} style={[Styles.rowBetween, { backgroundColor: index % 2 == 0 ? Color.gray : Color.white, paddingHorizontal: 10, paddingVertical: 10, }]}>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.mapel}</Text>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.benar}</Text>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.salah}</Text>
                            <Text style={[Styles.primaryFontText, { flex: 1, fontSize: width / 50, textAlign: 'center' }]}>{item.kosong}</Text>
                        </View>
                    )
                })}
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ListRingkasan);