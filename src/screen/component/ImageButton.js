import React, { Component } from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width } = Dimensions.get('window')

class ImageButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={styles.container}>
                <Image resizeMode='cover' style={styles.image} source={this.props.image} />
                <View style={styles.frontText}>
                    <Text style={[Styles.bigWhiteTextBold, { fontSize: 16 }]}>{this.props.title}</Text>
                    <Text style={Styles.whiteText}>{this.props.label}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}
const styles = {
   
    container: {
        // flex: 1,
        // justifyContent: 'center',
        flex:1,
        // paddingHorizontal:10,
        marginVertical:5,
        borderRadius:10,
        backgroundColor: Color.greenFill
    },
    frontText: {
        width:'100%', height: width / 2.5, backgroundColor: Color.blackOpacity,
        justifyContent:'flex-end',
        padding: 10, position: 'absolute',
        borderRadius:10,
    },

    image: {
        width:'100%', height: width / 2.5,
        borderRadius:10,
        // marginHorizontal:20
        // borderWidth:1, borderColor:Color.primary
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ImageButton);