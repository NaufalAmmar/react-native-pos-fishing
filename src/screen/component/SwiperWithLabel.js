import { dispatch as Redux } from '@rematch/core';
import moment from 'moment';
import React, { Component } from 'react';
import { Dimensions, Image, Text, TouchableOpacity, View } from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width } = Dimensions.get('window')

class SwiperWithLabel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    goNews(data) {
        this.root.navigate('News', data)
        Redux.component.setBlog(data)
    }
    render() {
        return (
            <Swiper style={styles.wrapper} height={width / 2.5}
                autoplay={true}
                // autoplayTimeout={5}
                automaticallyAdjustContentInsets={true}
                onMomentumScrollEnd={(e, state, context) => null
                    // console.log('index:', state.index)
                }
                dot={<View style={{ backgroundColor: Color.blackOpacity, width: 7, height: 7, borderRadius: 7, marginHorizontal: 3, marginTop: 3, marginBottom: 3 }} />}
                activeDot={<View style={{ backgroundColor: Color.secondaryButton, width: 10, height: 10, borderRadius: 10, marginHorizontal: 3, marginTop: 3, marginBottom: 3 }} />}
                paginationStyle={{
                    right: 0, bottom: 0
                }}
            >
                {this.props.slides.map((item, index) => {
                    return (
                        <TouchableOpacity onPress={item.content != null ? () => this.goNews(item) : null} key={index} style={styles.slide}>
                            <Image resizeMode='contain' style={styles.image} source={{ uri: item.photo_thumb_url }} />
                            <View style={styles.frontText}>
                                <Text style={[Styles.bigWhiteTextBold, { fontSize: 16 }]}>{item.title}</Text>
                                <Text style={Styles.whiteText}>{moment(item.upload_time).format("DD MMMM YYYY")}</Text>
                            </View>
                        </TouchableOpacity>
                    )
                })}

            </Swiper>
        )
    }
}
const styles = {
    wrapper: {
        // backgroundColor: Color.black,
        // borderBottomColor: Color.primary, borderBottomWidth: 1
    },
    slide: {
        // flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Color.greenFill
    },
    frontText: {
        width: '100%', backgroundColor: Color.blackOpacity,
        padding: 10, position: 'absolute', bottom: 0
    },

    image: {
        width, height: width / 2.5,
        // borderWidth:1, borderColor:Color.primary
    }
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(SwiperWithLabel);