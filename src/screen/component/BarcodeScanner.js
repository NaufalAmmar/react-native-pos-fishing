
import React, { Component } from 'react';
import { StyleSheet, ToastAndroid, View, Text, Platform, TouchableOpacity, Linking,Image, PermissionsAndroid } from 'react-native';
import { connect } from 'react-redux';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Color from '../utility/Color'
import { CameraKitCameraScreen } from 'react-native-camera-kit';
import Button from './Button';

class BarcodeScanner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            QR_Code_Value: '',

            startCamera: false,

        }
    }
    componentDidMount() {
    }
    openLink_in_browser = () => {

        Linking.openURL(this.state.QR_Code_Value);

    }
    onQR_Code_Scan_Done = (QR_Code) => {
        ToastAndroid.showWithGravityAndOffset(
            QR_Code,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50,
        )
        this.setState({ QR_Code_Value: QR_Code });
        console.log(QR_Code)
        this.setState({ startCamera: false });
    }
    open_QR_Code_Scanner = () => {

        var that = this;

        if (Platform.OS === 'android') {
            async function requestCameraPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.CAMERA, {
                        'title': 'Cukurin',
                        'message': 'Camera App needs access to your camera '
                    }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                        that.setState({ QR_Code_Value: '' });
                        that.setState({ startCamera: true });
                        // console.log('OPEN')
                    } else {
                        alert("CAMERA permission denied");
                    }
                } catch (err) {
                    alert("Camera permission err", err);
                    console.warn(err);
                }
            }
            requestCameraPermission();
        } else {
            that.setState({ QR_Code_Value: '' });
            that.setState({ startCamera: true });
        }
    }
    render() {
        if (!this.props.startCamera) {

            return (
                <View style={styles.MainContainer}>

                    <Text style={{ fontSize: 22, textAlign: 'center' }}>React Native Scan QR Code Example</Text>

                    <Text style={styles.QR_text}>
                        {this.state.QR_Code_Value ? 'Scanned QR Code: ' + this.state.QR_Code_Value : ''}
                    </Text>

                    {this.state.QR_Code_Value.includes("http") ?
                        <TouchableOpacity
                            onPress={this.openLink_in_browser}
                            style={styles.button}>
                            <Text style={{ color: '#FFF', fontSize: 14 }}>Open Link in default Browser</Text>
                        </TouchableOpacity> : null
                    }

                    <TouchableOpacity
                        onPress={this.open_QR_Code_Scanner}
                        style={styles.button}>
                        <Text style={{ color: '#FFF', fontSize: 14 }}>
                            Open QR Scanner
            </Text>
                    </TouchableOpacity>

                </View>
            );
        }
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>

                    <CameraKitCameraScreen
                        showFrame={false}
                        scanBarcode={true}
                        laserColor={'#FF3D00'}
                        frameColor={Color.primary}
                        colorForScannerFrame={'black'}
                        cameraOptions={{
                            // flashMode: 'auto',             // on/off/auto(default)
                            focusMode: 'on',               // off/on(default)
                            zoomMode: 'on',                // off/on(default)
                            ratioOverlay: '1:1',            // optional, ratio overlay on the camera and crop the image seamlessly
                            ratioOverlayColor: '#00000077' // optional
                        }}
                        onReadQRCode={event =>
                            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
                        }
                    />
                </View>
                <View style={[styles.overlay, styles.topOverlay]}>
                    <Text style={styles.scanScreenMessage}>Please scan the QR code for Check In.</Text>
                    {/* {this.state.data_qrcode != '' && (
                        <Text style={styles.textQrCode}>Code : {this.state.data_qrcode}</Text>
                    )} */}
                </View>
                <View style={[styles.overlayBox, styles.box]}>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                        <Image
                            source={require('../../../img/frame.png')}
                            style={styles.boxImage}
                        />
                    </View>
                </View>

                <View style={[styles.overlay, styles.bottomOverlay]}>
                    <Button
                        onPress={() => { this.root.goBack() }}
                        containerStyle={styles.enterBarcodeManualButton}
                        labelButton="Tutup"
                    />
                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    QR_text: {
        color: '#000',
        fontSize: 19,
        padding: 8,
        marginTop: 12
    },
    button: {
        backgroundColor: '#2979FF',
        alignItems: 'center',
        padding: 12,
        width: 300,
        marginTop: 14
    },
    container: {
        flex: 1
    },
    box: {
        width: '70%', height: '70%',
        // borderWidth: 2, borderColor: Color.white,
        // backgroundColor: Color.white,
        alignSelf: 'center', marginHorizontal: 20, marginVertical: 0
    },
    boxImage: {
        alignSelf: 'center',
        width: '100%',
        height: '100%',
        position: 'absolute',
        resizeMode: 'contain',
    },
    overlayBox: {
        position: 'absolute',
        // padding: 16,
        // right: '25%',
        // left: '25%',
        top: 20,
        alignSelf: 'center'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textQrCode: {
        color: Color.white,
        fontWeight: 'bold',
        fontSize: 20
    },
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center'
    },
    topOverlay: {
        top: 0,
        // flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    bottomOverlay: {
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    enterBarcodeManualButton: {
        padding: 15,
        // backgroundColor: 'white',
        borderRadius: 40
    },
    scanScreenMessage: {
        fontSize: 14,
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }

});

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(BarcodeScanner);
