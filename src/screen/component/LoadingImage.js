import React, { Component } from 'react'
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    View,
    Animated,
    Image,
    Easing
} from 'react-native'
import Color from '../utility/Color';
export default class LoadingImage extends Component {
    constructor() {
        super()
        this.spinValue = new Animated.Value(0)
        this.springValue = new Animated.Value(0.3)
        this.state = {
            text: "Mengambil data"
        }
    }
    componentDidMount() {
        this.spin();
        this.spring();
        this.setText();
    }

    spin() {
        this.spinValue.setValue(0)
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 2000,
                useNativeDriver: true, 
                easing: Easing.linear
            }
        ).start(() => this.spin())
    }
    spring() {
        this.springValue.setValue(0.3)
        Animated.spring(
            this.springValue,
            {
                useNativeDriver: true, 
                toValue: 1,
                friction: 1
            }
        ).start(() => this.spring())
    }
    setText() {
        setTimeout(() => {
            this.setState({ text: "Sedang mengambil data.." })
        }, 50000)
    }
    render() {
        let { text } = this.state
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        const spinBorder = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        return (
            <View style={styles.container}>
                <Animated.View style={{
                    borderWidth: 1,
                    borderColor: Color.primary,
                    borderRadius: 100,
                    transform: [{ scale: this.springValue }],
                    padding: 4,
                    opacity: 0.7
                }} >
                    <Animated.Image
                        style={{
                            width: 40,
                            height: 40,
                            transform: [{ rotate: spin }],
                            resizeMode: 'contain',
                            opacity: 0.7
                        }}
                        source={require('../../../img/logo.png')}
                    />
                </Animated.View>
                <Animated.Text style={{
                    transform: [{ scale: this.springValue }],
                    padding: 4,
                    opacity: 0.3
                }}>{text}</Animated.Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 8,

    }
})