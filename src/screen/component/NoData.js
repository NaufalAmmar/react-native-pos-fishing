import React, { Component } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color';
import Styles from '../utility/Style';
const { width, height } = Dimensions.get('window')

class NoData extends Component {
    constructor(props) {
        super(props)
        this.root = this.props.component.root;
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', alignSelf: 'center', height: 150, justifyContent: 'center', opacity: 0.6 }}>
                <Image
                    source={require('../../../img/undraw_no_data.png')}
                    style={[Styles.center, { resizeMode: 'contain', alignSelf: 'center', width: width / 3, height: width / 3, }]}
                />
                {this.props.children}
            </View>
        );
    }
}


function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(NoData);