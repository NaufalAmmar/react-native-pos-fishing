import React, { Component } from 'react';
import { Dimensions, Modal, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
const { width, height } = Dimensions.get('window')

class PickerModal extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            modalLoading: false,
            modalPicker: false,
            selectedValue: ''
        }
    }

    componentDidMount() {
        let placeholder = this.props.placeholder != null ? this.props.placeholder : ''
        setTimeout(() => {
            if (this.props.data.length > 0) {
                this.setState({ selectedValue: this.props.data[0].label != null ? placeholder + " (" + this.props.data[0].label + ")" : placeholder });
            } else {
                this.setState({ selectedValue: placeholder });
            }
        }, 200)
    }

    selected(id, index, item) {
        this.setState({ modalPicker: false, selectedValue: item.label })
        this.props.onValueChange(id, index)
    }
    picker() {
        let { data } = this.props
        let { selectedValue } = this.state
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalPicker}
                onRequestClose={() => this.setState({ modalPicker: false })}>
                <TouchableOpacity onPress={() => this.setState({ modalPicker: false })}
                    style={[this.props.style, { flex: 1, backgroundColor: 'rgba(0,0,0,0.3)', justifyContent: 'center', paddingHorizontal: 24 }]}
                >
                    <ScrollView
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        // style={{ flex:1, alignSelf:'center' }}
                        keyboardShouldPersistTaps='always'>
                        <View style={[Styles.container, { backgroundColor: 'transparent' }]}>
                            <View style={{ backgroundColor: Color.white, padding: 16, paddingHorizontal: 32, marginVertical: height / 4 }}>
                                {data.map((item, index) => {
                                    return (
                                        <TouchableOpacity key={index} onPress={() => this.selected(item.id, index, item)} style={{ marginVertical: 4, borderBottomWidth: 1, borderBottomColor: Color.grayWhite, paddingBottom: 8 }}>
                                            <Text style={Styles.primaryFontText}>{item.label.length >= Math.round(width / 8.5) ? item.label.substring(0, Math.round(width / 8.5)) + ".." : item.label}</Text>
                                        </TouchableOpacity>
                                    )
                                })}
                            </View>
                        </View>
                    </ScrollView>
                </TouchableOpacity>
            </Modal>
        )
    }
    openPicker() {
        if (this.props.data != null && this.props.data.length > 0) {
            this.setState({ modalPicker: true })
        } else {
            alert("Data is empty")
        }
    }
    render() {
        return (
            <View style={[Styles.column, { marginVertical: 8, marginHorizontal: 16 }]}>
                {this.props.label != null && (
                    <Text style={[Styles.primaryFontText, { flex: 1 }]}>{this.props.label.length >= Math.round(width / 8.5) ? this.props.label.substring(0, Math.round(width / 8.5)) + ".." : this.props.label}</Text>
                )}
                <View style={Styles.picker}>
                    <TouchableOpacity onPress={() => this.openPicker()} style={[Styles.rowBetween, { alignItems: 'center', paddingHorizontal: 8 }]}>
                        {this.props.iconLeft != null && (
                            <MaterialCommunityIcons
                                name={this.props.iconLeft}
                                size={20}
                                style={{ marginRight: 4, }}
                                color={Color.primaryFont}
                            />
                        )}
                        <Text style={[Styles.primaryFontText, { flex: 1 }]}>{this.state.selectedValue.length >= Math.round(width / 8.5) ? this.state.selectedValue.substring(0, Math.round(width / 8.5)) + ".." : this.state.selectedValue}</Text>
                        <MaterialCommunityIcons
                            name="chevron-down"
                            size={20}
                            // style={{flex:1,}}
                            color={Color.primaryFont}
                        />
                        {this.picker()}
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(PickerModal);