import React, { Component } from 'react';
import { Keyboard, Text, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../utility/Color';

class Footer extends Component {
    constructor(props) {
        super(props);
        let root = this.props.parent.root.state;
        this.state = {
            selected: root.selectedPage,
            beforePage: root.backStack[root.backStack.length - 1],
            bottom: 0,
            footer: [
                { id: 0, title: 'Beranda', icon: 'coffee', page: 'Dashboard' },
                { id: 1, title: 'ZenBOS', icon: 'puzzle', page: 'ZenbosList' },
                { id: 2, title: 'Analisis', icon: 'notebook', page: 'Analisis' },
                { id: 3, title: 'Statistik', icon: 'poll-box', page: 'Statistik' },
                { id: 4, title: 'Profil', icon: 'account-box', page: 'Profile' },
            ],
        };
    }
    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow = () => {
        this.setState({ bottom: -100 });
    }

    _keyboardDidHide = () => {
        this.setState({ bottom: 0 });
    }
    render() {
        let { selected, footer, beforePage } = this.state
        return (
            <View style={[styles.menu, { bottom: this.state.bottom }]}>
                {footer.map((item, key) => {
                    // if (beforePage != item.page) {
                    return (
                        <TouchableOpacity disabled={item.page == selected ? true : false} key={key} style={styles.button} onPress={() => {
                            let parent = this.props.parent;
                            parent.root.changePage(item.page);
                            // alert(beforePage)
                        }}>
                            <MaterialCommunityIcons
                                name={item.icon}
                                size={25}
                                color={this.state.selected == item.page ? Color.primary : Color.grayFill}
                            />
                            <Text style={[styles.textButton, { color: this.state.selected == item.page ? Color.primary : Color.grayFill }]}>
                                {item.title}
                            </Text>
                        </TouchableOpacity>
                    )
                    // }
                })}
                {/* {(beforePage == 'ZenbosList' || beforePage == 'Dashboard' || beforePage == 'Analisis' || beforePage == 'Statistik' || beforePage == 'Profile' || beforePage != '') && (
                    <TouchableOpacity style={[styles.button, { backgroundColor: Color.white }]} onPress={() => {
                        let parent = this.props.parent;
                        parent.root.goBack();
                        // alert(beforePage)
                    }}>
                        <Ionicons
                            name={"ios-arrow-back"}
                            size={25}
                            color={Color.secondaryButton}
                        />
                        <Text style={[styles.textButton, { color: Color.secondaryButton }]}>
                            Kembali
                    </Text>
                    </TouchableOpacity>
                )} */}
            </View>
        )
    }
}
const styles = {
    menu: {
        flex: 2, flexDirection: 'row',
        position: 'absolute',
        backgroundColor: Color.white,
        borderTopWidth: 1, borderTopColor: Color.primary,
        elevation: 3, paddingVertical: 5, paddingHorizontal: 5
    },
    button: {
        flex: 1, flexDirection: 'column',
        justifyContent: 'center', alignItems: 'center',
        padding: 5, marginHorizontal: 5
    },
    textButton: {
        fontSize: 10,
        color: 'white'
    }
}
export default Footer;