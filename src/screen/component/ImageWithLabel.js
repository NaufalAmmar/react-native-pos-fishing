import React, { Component } from 'react';
import { Alert, Dimensions, Text, TouchableOpacity, View } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import PhotoUploader from '../utility/PhotoUploader';
import Styles from '../utility/Style.js';
import PhotoFrame from './PhotoFrame';
const { height, width } = Dimensions.get('window');

class ImageWithLabel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            foto: '',
            defaultImage: require('../../../img/id.jpg')
        }
        this.root = this.props.component.root;
        this.user = this.props.component.user;
    }
    ambilFoto() {
        this.setState({ isLoading: true });

        PhotoUploader.upload('foto_vendor', this.user.user.username, 'avatar').then((result) => {
            // console.log(result);
            this.setState({ isLoading: false, foto: result.data });
            this.props.result(result.data)
        }).catch((error) => {
            Alert.alert("Information", error.message);
            this.setState({ isLoading: false });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={[Styles.blackTextBold, { fontSize: 15, marginVertical: 5 }]}>{this.props.label}</Text>
                <View style={styles.formPhoto}>
                    {this.props.source == '' && (
                        <PhotoFrame
                            loadingStatus={this.state.isLoading}
                            imageUrl={this.state.foto.length == 0 ? this.state.defaultImage : { uri: this.state.foto }}
                            onPress={() => {
                                this.ambilFoto();
                            }}
                        />
                    )}
                    {this.props.source != '' && (
                        <PhotoFrame
                            loadingStatus={this.state.isLoading}
                            imageUrl={this.props.source == '' ? this.state.defaultImage : { uri: this.props.source }}
                            onPress={() => {
                                this.ambilFoto();
                            }}
                        />
                    )}

                    {this.state.foto.length > 0 && (
                        <TouchableOpacity style={styles.iconEdit}
                            onPress={() => {
                                this.ambilFoto();
                            }}>
                            <MaterialIcons
                                name="edit"
                                color={Color.black}
                                size={30}
                                style={{ alignSelf: 'center' }}
                            />
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        )
    }
}
const styles = {
    container: {
        flex: 1
    },
    formPhoto: {
        flex: 1, height: height / 3, borderRadius: 0, borderWidth: 1, borderColor: Color.border,
        alignItems: 'center', justifyContent: 'center'
    },
    iconEdit: {
        flex: 1,
        padding: 5,
        justifyContent: 'center', alignItems: 'center',
        position: 'absolute',
        right: 5, top: 5,
        // backgroundColor: Color.white
    },
}
function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(ImageWithLabel);