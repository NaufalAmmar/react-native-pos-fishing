import React, { Component } from 'react';
import { View, Text, Alert, Image, ActivityIndicator, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';
import Url from '../utility/Url';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { dispatch as Redux } from '@rematch/core';
const { height, width } = Dimensions.get('window');
import Lightbox from 'react-native-lightbox';
import FitImage from 'react-native-fit-image';
import Button from './Button';
import moment from 'moment';

class VoucherContent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            clicked: 0,
            defaultImage: require('../../../img/photo.png'),
            isLoading: false
        }
        this.root = this.props.component.root;
        this.data_user = this.props.component.user;
    }
    confirmClick(data) {
        Alert.alert(
            'Peringatan',
            'Konfirmasi Kedatangan PE ?',
            [
                {
                    text: 'Batal', onPress: () => {
                        null
                    }
                }, {
                    text: 'Konfirmasi', onPress: () => {
                        let log = data.pe_log_id;
                        // console.log('pe log', log)
                        this.setState({ isLoading: true })
                        Url.konfirmasiVisit(log).then((response) => {
                            // let dataKonfirmasi = response.data.data;
                            this.setState({ isLoading: false });
                            Alert.alert("Informasi", "Terima Kasih atas konfirmasi anda");
                            this.inbox = this.props.component.inbox
                            this.setState({ isLoading: false })
                            this.inbox.loadInbox()
                            // console.log('data konfirmasi:', dataKonfirmasi);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                alert("Network Error.")
                                this.setState({ isLoading: false });
                            }
                            alert(error.response.data.data);
                            this.setState({ isLoading: false });
                        });
                    }
                }],
            { cancelable: false }
        )

    }
    abortClick(data) {
        Alert.alert(
            'Peringatan',
            'Apakah PE tidak mengunjungi anda ?',
            [
                {
                    text: 'Batal', onPress: () => {
                        null
                    }
                }, {
                    text: 'Iya', onPress: () => {
                        let log = data.pe_log_id;
                        // console.log('pe log', log)
                        this.setState({ isLoading: true })
                        Url.rejectVisit(log).then((response) => {
                            // let dataKonfirmasi = response.data.data;
                            this.setState({ isLoading: false });
                            Alert.alert("Informasi", "Terima Kasih atas konfirmasi anda");
                            this.inbox = this.props.component.inbox
                            this.setState({ isLoading: false })
                            this.inbox.loadInbox()
                            // console.log('data konfirmasi:', dataKonfirmasi);
                        }).catch((error) => {
                            // console.log(error, error.response);
                            if (error.response == null) {
                                alert("Network Error.")
                                this.setState({ isLoading: false });
                            }
                            alert(error.response.data.data);
                            this.setState({ isLoading: false });
                        });
                    }
                }],
            { cancelable: false }
        )
    }
    gunakanVoucher(){
        null
    }
    render() {
        if (this.props.data.length != 0 && this.props.data != null) {
            return (
                <View style={styles.menu} >
                    {this.props.data.map((item, index) => {
                        let page = null
                        if (item.type == 'INBOX' && item.type == 'CONFIRM') {
                            page = null
                        } else if (item.type == 'NEWS') {
                            page = 'Dashboard'
                        } else if (item.type == 'HADIAH') {
                            page = 'LihatHadiah'
                        } else {
                            page = null
                        }
                        // console.log(item, page)
                        return (
                            <View key={index} style={styles.box} >
                                {item.imageUrl == null && item != null && (
                                    <View>
                                        <View style={Styles.row}>                                           
                                            <View style={{ flex: 1 }}>
                                                <View style={Styles.rowBetween}>
                                                    <Text style={[Styles.blackTextBold, { fontSize: 12, marginBottom: 5, flexWrap: 'wrap' }]}>{item.judul.length > 34 ? item.judul.substr(0, 34) + ".." : item.judul}</Text>
                                                </View>
                                                <Text style={[Styles.blackText, { fontSize: 12, marginBottom: 5 }]}>{item.keterangan}</Text>
                                                <Text style={[Styles.blackTextBold, { alignSelf: 'flex-end', fontSize: 10 }]}>{item.created_at}</Text>
                                                
                                            </View>
                                        </View>
                                    </View>
                                )}
                                {item.imageUrl != null && item != null && (
                                    <View style={{ flex: 1, borderRadius: 20 }}>
                                        {item.imageUrl != null && (
                                            <Lightbox underlayColor="white" style={{ borderRadius: 20 }}>
                                                <FitImage
                                                    borderRadius={20}
                                                    resizeMode="cover"
                                                    source={{ uri: item.imageUrl }}
                                                    style={styles.inboxImage}
                                                />
                                            </Lightbox>
                                        )}
                                        <View style={[Styles.rowBetween, { alignItems: 'center', backgroundColor: Color.white, paddingHorizontal: 20, paddingVertical: 10, position: 'absolute', bottom: 0, borderBottomStartRadius: 20, borderBottomEndRadius: 20 }]}>
                                            <View style={{ flex: 1 }}>
                                                <Text style={[Styles.blackText, { fontSize: 15, flexWrap: 'wrap' }]}>{item.label.length > 34 ? item.label.substr(0, 34) + ".." : item.label}</Text>
                                                <Text style={[Styles.primaryFontText, { alignSelf: 'flex-start', fontSize: 10 }]}>{moment(item.time).format("DD MMMM YYYY HH:mm:ss" )}</Text>
                                            </View>
                                            <Button
                                                labelButton="Gunakan"
                                                onPress={() => this.gunakanVoucher()}
                                                backgroundColor={Color.secondaryButton}
                                            // iconLeft={true}
                                            // iconLeftName="md-eye"
                                            />
                                        </View>
                                    </View>
                                )}
                            </View>
                        )
                    })
                    }
                </View>
            )
        }
    }
}
const styles = {
    menu: {
        backgroundColor: Color.white,
        flex: 1,
        paddingHorizontal: 2
    },
    icon: {
        width: 40, height: 40,
        padding: 10,
        backgroundColor: Color.primary,
        borderRadius: 10
    },
    redButton: {
        padding: 5, borderRadius: 5,
        borderColor: Color.redFill, borderWidth: 1,
        marginRight: 5,
    },
    greenButton: {
        padding: 5, borderRadius: 5,
        borderColor: Color.greenFill, borderWidth: 1
    },
    inboxImage: {
        // height:400,
        // resizeMode:'contain',
        borderRadius: 20,
        // borderWidth: 1, borderColor: 'rgba(0,0,0,0.2)'
    },
    box: {
        flex: 1,
        backgroundColor: Color.white,
        paddingVertical: 5,
        // paddingHorizontal: 10,
        marginVertical: 5,
        // borderRadius: 10,
        // elevation: 5,
        borderBottomWidth: 1, borderColor: Color.gray,

    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(VoucherContent);