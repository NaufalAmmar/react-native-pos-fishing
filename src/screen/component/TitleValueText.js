import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import Color from '../utility/Color.js';
import Styles from '../utility/Style.js';

class TitleValueText extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.root = this.props.component.root;
    }
    render() {
        return (
            <View style={styles.view}>
                <Text style={[Styles.blackText, { fontWeight: this.props.titleWeight, fontSize: this.props.titleSize != null ? this.props.titleSize : 13, textAlign: this.props.textAlign, color: this.props.titleColor }]}>{this.props.title}</Text>
                {this.props.value != null && (
                    <TouchableOpacity onPress={this.props.labelOnPress != null ? this.props.labelOnPress : null}>
                        <Text style={[Styles.blackText, { fontWeight: this.props.valueWeight, flexWrap: 'wrap', fontSize: this.props.valueSize != null ? this.props.valueSize : 15, textAlign: this.props.textAlign, color: this.props.labelOnPress != null ? Color.secondaryButton : this.props.valueColor }]}>{this.props.value}</Text>
                        <Text style={[Styles.blackText, { fontWeight: this.props.childWeight, flexWrap: 'wrap', fontSize: this.props.childSize != null ? this.props.childSize : 15, textAlign: this.props.textAlign, color: this.props.labelOnPress != null ? Color.secondaryButton : this.props.childColor }]}>{this.props.child}</Text>
                    </TouchableOpacity>
                )}
            </View>
        )
    }
}
const styles = {
    view: {
        flex: 1, marginVertical: 5
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}
export default connect(
    mapStateToProps
)(TitleValueText);