import Root from './src/screen/Root';
import SplashScreen from './src/screen/component/SplashScreen';
// user
import SignIn from './src/screen/user/SignIn';
import SignUp from './src/screen/user/SignUp';
import Mainboard from './src/screen/user/Mainboard';
import SurveyCarwash from './src/screen/user/SurveyCarwash';
import ListLaundry from './src/screen/user/ListLaundry';
// owner
import SignUpOwner from './src/screen/owner/SignUpOwner';
import TambahKurir from './src/screen/owner/TambahKurir';
import DataCabang from './src/screen/owner/DataCabang';
import DataLayanan from './src/screen/owner/DataLayanan';

// kurir
import { createStackNavigator } from 'react-navigation-stack';

import { createAppContainer } from 'react-navigation';
console.disableYellowBox = true;
const MainNavigator = createStackNavigator(

    {
        Root: { screen: Root },
        SplashScreen: { screen: SplashScreen },
        // user
        SignIn: { screen: SignIn },
        SignUp: { screen: SignUp },
        Mainboard: { screen: Mainboard },
        SurveyCarwash: { screen: SurveyCarwash },
        ListLaundry: { screen: ListLaundry },
        // owner
        SignUpOwner: { screen: SignUpOwner },
        TambahKurir: { screen: TambahKurir },
        DataCabang: { screen: DataCabang },
        DataLayanan: { screen: DataLayanan },
        // kurir
    },
    {
        initialRouteName: 'Root',
        headerMode: 'none',
        defaultNavigationOptions: {
            header: null
        },
    }
);
const App = createAppContainer(MainNavigator);
export default App

